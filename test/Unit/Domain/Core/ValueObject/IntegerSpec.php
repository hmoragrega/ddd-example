<?php

namespace Unit\Example\Domain\Core\ValueObject;

use Example\Domain\Core\Exception\ValidationException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class IntegerSpec
 *
 * @package Unit\Example\Domain\Core\ValueObject
 * @mixin \Example\Domain\Core\ValueObject\Integer
 */
class IntegerSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(1);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Example\Domain\Core\ValueObject\Integer');
    }
    
    function it_fails_with_other_types()
    {
        $this->beConstructedWith('1');
        $this->shouldThrow(new ValidationException('The value is not a integer'))->duringInstantiation();
    }
    
    function it_can_return_the_value()
    {
        $this->getValue()->shouldReturn(1);
    }
    
    function it_can_be_incremented()
    {
        $this->increment();
        $this->getValue()->shouldReturn(2);
    }
    
    function it_can_be_decremented()
    {
        $this->decrement();
        $this->getValue()->shouldReturn(0);
    }

    function it_can_be_counted()
    {
        $this->count()->shouldReturn(1);
    }
    
    function it_knows_his_range()
    {
        $this->isPositive()->shouldBe(true);
        $this->isZero()->shouldBe(false);
        $this->isNegative()->shouldBe(false);
        
        $this->decrement();

        $this->isPositive()->shouldBe(false);
        $this->isZero()->shouldBe(true);
        $this->isNegative()->shouldBe(false);
        
        $this->decrement();

        $this->isPositive()->shouldBe(false);
        $this->isZero()->shouldBe(false);
        $this->isNegative()->shouldBe(true);
    }
}
