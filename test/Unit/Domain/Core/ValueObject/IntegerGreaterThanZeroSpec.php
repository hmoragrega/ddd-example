<?php

namespace Unit\Example\Domain\Core\ValueObject;

use Example\Domain\Core\Exception\ValidationException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class IntegerGreaterThanZeroSpec
 *
 * @package Unit\Example\Domain\Core\ValueObject
 * @mixin \Example\Domain\Core\ValueObject\IntegerGreaterThanZero
 */
class IntegerGreaterThanZeroSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(1);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Example\Domain\Core\ValueObject\IntegerGreaterThanZero');
    }

    function it_fails_with_smaller_values()
    {
        $this->beConstructedWith(0);
        $this->shouldThrow(new ValidationException('The value has to be equal or greater than 1'))->duringInstantiation();
    }
}
