<?php

namespace Unit\Example\Domain\Core\ValueObject;

use Example\Domain\Core\Exception\ValidationException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class StringNotEmptySpec
 *
 * @package Unit\Example\Domain\Core\ValueObject
 * @mixin \Example\Domain\Core\ValueObject\StringNotEmpty
 */
class StringNotEmptySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith('example');
        $this->shouldHaveType('Example\Domain\Core\ValueObject\StringNotEmpty');
    }
    
    function it_is_fails_with_empty_string()
    {
        $this->beConstructedWith('');
        $this->shouldThrow(new ValidationException('The string cannot be empty'))->duringInstantiation();
    }
}
