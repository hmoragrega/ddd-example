<?php

namespace Unit\Example\Domain\Core\ValueObject;

use Example\Domain\Core\Exception\ValidationException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class BooleanSpec
 *
 * @package Unit\Example\Domain\Core\ValueObject
 * @mixin \Example\Domain\Core\ValueObject\Boolean
 */
class BooleanSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(true);
    }

    function it_is_initializable()
    {
        $this->beConstructedWith(false);
        $this->shouldHaveType('Example\Domain\Core\ValueObject\Boolean');
    }

    function it_fails_for_other_types()
    {
        $this->beConstructedWith('true');
        $this->shouldThrow(new ValidationException('The value is not a boolean'))->duringInstantiation();
    }
    
    public function it_knows_when_is_true()
    {
        $this->isTrue()->shouldBe(true);
        $this->isFalse()->shouldBe(false);
    }
}
