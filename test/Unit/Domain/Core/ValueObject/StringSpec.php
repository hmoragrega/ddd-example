<?php

namespace Unit\Example\Domain\Core\ValueObject;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class StringSpec
 *
 * @package Unit\Example\Domain\Core\ValueObject
 * @mixin \Example\Domain\Core\ValueObject\String
 */
class StringSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('漢字はユニコード');
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Example\Domain\Core\ValueObject\String');
    }
    
    function it_can_be_count_the_characters()
    {
        $this->count()->shouldReturn(8);
    }

    function it_can_get_the_size()
    {
        $this->size()->shouldReturn(24);
    }
}
