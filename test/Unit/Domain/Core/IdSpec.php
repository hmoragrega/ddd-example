<?php

namespace Unit\Example\Domain\Core;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class IdSpec
 *
 * @package Unit\Example\Domain\Core
 * @mixin \Example\Domain\Core\Id
 */
class IdSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(1);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Example\Domain\Core\Id');
    }
    
    function it_fails_for_other_types()
    {
        $this->beConstructedWith('1');
        $this->shouldThrow('The id has to be null or a positive integer');
    }
    
    function it_fails_non_positive_integer_values()
    {
        $this->beConstructedWith(0);
        $this->shouldThrow('The id has to be a positive integer');
    }
    
    function it_knows_if_it_is_null()
    {
        $this->beConstructedWith(null);
        $this->isNull()->shouldBe(true);
    }
}
