<?php

namespace Unit\Example\Domain\User;

use Example\Domain\Core\Exception\ValidationException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class UserLevelSpec
 *
 * @package Unit\Example\Domain\User
 * @mixin \Example\Domain\User\UserLevel
 */
class UserLevelSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('free');
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('Example\Domain\User\UserLevel');
    }
    
    function it_fails_with_other_values()
    {
        $this->beConstructedWith('foo');
        $this->shouldThrow(new ValidationException('The user level has to be one of "free","admin"'))->duringInstantiation();
    }

    function it_can_be_modified()
    {
        $this->convertToFree();

        $this->isFree()->shouldBe(true);
        $this->isAdmin()->shouldBe(false);
        
        $this->convertToAdmin();

        $this->isFree()->shouldBe(false);
        $this->isAdmin()->shouldBe(true);
    }
}
