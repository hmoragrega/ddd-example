<?php

namespace Example\Application;

use Example\Domain\Core\Command\CommandInterface;

/**
 * Class AbstractCommandDecorator
 *
 * @package Example\Application
 */
abstract class AbstractCommandDecorator implements CommandInterface
{
    /**
     * @var CommandInterface
     */
    protected $command;

    /**
     * AbstractCommandDecorator constructor.
     *
     * @param CommandInterface $command
     */
    public function __construct(CommandInterface $command)
    {
        $this->command = $command;
    }

    /**
     * @return CommandInterface
     */
    protected function getCommand()
    {
        return $this->command instanceof CommandDecoratorInterface
            ? $this->command->getDecoratedCommand()
            : $this->command;
    }

    /**
     * @return string
     */
    protected function getCommandClass()
    {
        return get_class($this->getCommand());
    }
}
