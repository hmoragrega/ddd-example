<?php

namespace Example\Application\Monitoring;

/**
 * Interface MonitorInterface
 *
 * @package Example\Application\Monitoring
 */
interface MonitorInterface
{
    /**
     * @param       $metric
     * @param array $tags
     *
     * @return mixed
     */
    public function increment($metric, $tags = []);

    /**
     * @param       $metric
     * @param array $tags
     *
     * @return mixed
     */
    public function start($metric, $tags = []);

    /**
     * @param       $metric
     * @param array $tags
     *
     * @return mixed
     */
    public function end($metric, $tags = []);
}