<?php

namespace Example\Application\DependencyInjection;

/**
 * Interface ServiceLocatorInterface
 *
 * @package Example\Application\DependencyInjection
 */
interface ServiceLocatorInterface
{
    /**
     * @param string $service
     *
     * @return mixed
     *
     * @throw \Exception if the dependency cannot be resolved
     */
    public function get($service);
}