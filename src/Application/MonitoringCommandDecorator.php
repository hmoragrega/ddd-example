<?php

namespace Example\Application;

use Example\Application\Monitoring\MonitorInterface;
use Example\Domain\Core\Command\CommandInterface;
use Example\Domain\Core\Exception\CriticalException;
use Example\Domain\Core\Exception\ErrorException;
use Example\Domain\Core\ParametersInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class CriticalFailuresUseCaseDecorator
 *
 * @package Example\Application\Domain
 */
class MonitoringCommandDecorator extends AbstractCommandDecorator
{
    /**
     * @var CommandInterface
     */
    private $metric;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MonitorInterface
     */
    private $monitor;

    /**
     * MonitoringCommandDecorator constructor.
     *
     * @param CommandInterface $metric
     * @param CommandInterface $command
     * @param LoggerInterface  $logger
     * @param MonitorInterface $monitor
     */
    public function __construct($metric, CommandInterface $command, LoggerInterface $logger, MonitorInterface $monitor)
    {
        parent::__construct($command);
        $this->metric = $metric;
        $this->logger = $logger;
        $this->monitor = $monitor;
    }


    /**
     * @param ParametersInterface $parameters
     *
     * @return mixed
     * @throws CriticalException
     * @throws ErrorException
     * @throws \Exception
     */
    public function execute(ParametersInterface $parameters)
    {
        try {
            $this->monitor->start($this->metric);
            $result = $this->command->execute($parameters);
            $this->monitor(LogLevel::DEBUG, $parameters);

        } catch (CriticalException $exception) {
            $this->monitor(LogLevel::CRITICAL, $parameters, $exception);
            throw $exception;

        } catch (ErrorException $exception) {
            $this->monitor(LogLevel::ERROR, $parameters, $exception);
            throw $exception;

        } catch (\Exception $exception) {
            $this->monitor(LogLevel::ALERT, $parameters, $exception);
            throw $exception;
        }

        return $result;
    }

    private function monitor($level, ParametersInterface $parameters, \Exception $exception = null)
    {
        $this->logger->log($level, $this->getCommandClass(), ['parameters' => $parameters, 'exception' => $exception]);
        $this->monitor->end($this->metric, ['level' => $level]);
    }
}
