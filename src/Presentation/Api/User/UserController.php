<?php

namespace Example\Presentation\Api\User;

use Example\Infrastructure\Domain\Core\NativeParametersAdapter;
use Example\Infrastructure\Domain\Core\SymfonyParametersAdapter;
use Example\Presentation\Api\JsonControllerTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 *
 * @package Example\Presentation\Api\User
 */
class UserController
{
    use JsonControllerTrait;

    /**
     * @param $userId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getUserProfile($userId)
    {
        return $this->execute(
            $this->commands->get('command.user.getUserProfile'),
            new NativeParametersAdapter(['userId' => (int) $userId])
        );
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request)
    {
        return $this->execute(
            $this->commands->get('command.user.create'),
            new SymfonyParametersAdapter($request->request)
        );
    }

    /**
     * @param $userId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete($userId)
    {
        return $this->execute(
            $this->commands->get('command.user.delete'),
            new NativeParametersAdapter(['userId' => (int) $userId])
        );
    }

    /**
     * @param Request $request
     * @param         $userId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function update(Request $request, $userId)
    {
        $request->request->set('userId', (int) $userId);

        return $this->execute(
            $this->commands->get('command.user.update'),
            new SymfonyParametersAdapter($request->request)
        );
    }
}
