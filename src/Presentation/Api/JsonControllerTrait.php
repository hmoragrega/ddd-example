<?php

namespace Example\Presentation\Api;

use Example\Application\DependencyInjection\ServiceLocatorInterface;
use Example\Domain\Core\Command\CommandInterface;
use Example\Domain\Core\Exception\EntityNotFoundException;
use Example\Domain\Core\Exception\ValidationException;
use Example\Domain\Core\ParametersInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JsonControllerTrait
 *
 * @package Example\Presentation\Api
 */
trait JsonControllerTrait
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $commands;

    /**
     * JsonControllerTrait constructor.
     *
     * @param ServiceLocatorInterface $commands
     */
    public function __construct(ServiceLocatorInterface $commands)
    {
        $this->commands = $commands;
    }

    /**
     * @param CommandInterface    $command
     * @param ParametersInterface $parameters
     *
     * @return JsonResponse
     */
    public function execute(CommandInterface $command, ParametersInterface $parameters)
    {
        try {
            return new JsonResponse($command->execute($parameters));

        } catch (EntityNotFoundException $exception) {
            return $this->notFound($exception->getMessage());

        } catch (ValidationException $exception) {
            return $this->badRequest($exception->getErrors());

        } catch (\Exception $exception) {
            return $this->serverError($exception->getMessage());
        }
    }

    /**
     * @param null $message
     *
     * @return JsonResponse
     */
    private function notFound($message)
    {
        return $this->error($message, Response::HTTP_NOT_FOUND);
    }

    /**
     * @param null $message
     *
     * @return JsonResponse
     */
    protected function badRequest($message)
    {
        return $this->error($message, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param null $message
     *
     * @return JsonResponse
     */
    protected function serverError($message)
    {
        return $this->error($message, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param $message
     * @param $code
     *
     * @return JsonResponse
     */
    protected function error($message, $code)
    {
        return new JsonResponse(['error' => $message, 'code' => $code], $code);
    }
}
