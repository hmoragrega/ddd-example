<?php

namespace Example\Presentation\Api\Media;

use Example\Infrastructure\Domain\Core\NativeParametersAdapter;
use Example\Infrastructure\Domain\Core\SymfonyParametersAdapter;
use Example\Presentation\Api\JsonControllerTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PhotoController
 *
 * @package Example\Presentation\Api\Media
 */
class PhotoController
{
    use JsonControllerTrait;

    /**
     * @param $userId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAllByUser($userId)
    {
        return $this->execute(
            $this->commands->get('command.photo.getAllByUser'), 
            new NativeParametersAdapter(['userId' => (int) $userId])
        );
    }

    /**
     * @param Request $request
     * @param         $userId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request, $userId)
    {
        $request->request->set('userId', (int) $userId);

        return $this->execute(
            $this->commands->get('command.photo.create'),
            new SymfonyParametersAdapter($request->request)
        );
    }
}
