<?php

namespace Example\Presentation\Web\User;

use Example\Application\DependencyInjection\ServiceLocatorInterface;
use Example\Application\User\GetProfileUseCase;
use Example\Domain\Core\Exception\EntityNotFoundException;
use Example\Domain\Core\Exception\ValidationException;
use Example\Infrastructure\Domain\Core\NativeParametersAdapter;
use Example\Presentation\Web\AbstractWebController;
use Example\Presentation\Web\TemplateEngineInterface;

class ProfileController extends AbstractWebController
{
    private $useCases;

    public function __construct(TemplateEngineInterface $template, ServiceLocatorInterface $useCases)
    {
        parent::__construct($template);
        $this->useCases = $useCases;
    }

    public function get($userId)
    {
        try {
            /** @var GetProfileUseCase $getProfile */
            $getProfile = $this->useCases->get('user.profile.get.use.case');
            $profile = $getProfile->execute(new NativeParametersAdapter(['userId' => $userId]));

            return $this->template()->render('user/profile/profile.twig', ['profile' => $profile]);
        
        } catch (EntityNotFoundException $exception) {
            return $this->notFound($exception);
            
        } catch (ValidationException $exception) {
            return $this->badRequest($exception);
            
        } catch (\Exception $exception) {
            return $this->serverError($exception);
        }
    }
}
