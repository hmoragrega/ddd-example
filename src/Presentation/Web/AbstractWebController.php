<?php

namespace Example\Presentation\Web;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractWebController
{
    /** @var TemplateEngineInterface  */
    private $template;

    public function __construct(TemplateEngineInterface $template)
    {
        $this->template = $template;
    }

    /**
     * @return TemplateEngineInterface
     */
    public function template()
    {
        return $this->template;
    }

    protected function notFound($exception = null)
    {
        return new Response(
            $this->template->render('error/not_found.twig', ['exception' => $exception]),
            Response::HTTP_NOT_FOUND);
    }

    protected function badRequest($exception = null)
    {
        return new Response(
            $this->template->render('error/bad_request.twig', ['exception' => $exception]),
            Response::HTTP_BAD_REQUEST
        );
    }

    protected function serverError($exception = null)
    {
        return new Response(
            $this->template->render('error/server_error.twig', ['exception' => $exception]), 
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
