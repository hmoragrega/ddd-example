<?php

namespace Example\Presentation\Web;

interface TemplateEngineInterface
{
    public function render($template, $variables = []);
}