<?php

namespace Example\Infrastructure\Application\Monitoring;

use Example\Application\Monitoring\MonitorInterface;

class DummyMonitor implements MonitorInterface
{
    public function increment($metric, $tags = [])
    {
        // do nothing
    }

    public function start($metric, $tags = [])
    {
        // do nothing
    }

    public function end($metric, $tags = [])
    {
        // do nothing
    }
}
