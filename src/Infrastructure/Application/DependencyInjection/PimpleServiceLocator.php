<?php

namespace Example\Infrastructure\Application\DependencyInjection;

use Example\Application\DependencyInjection\ServiceLocatorInterface;

class PimpleServiceLocator implements ServiceLocatorInterface
{
    /**
     * @var \Pimple
     */
    private $pimple;

    public function __construct(\Pimple $pimple)
    {
        $this->pimple = $pimple;
    }

    public function get($service)
    {
        return $this->pimple[$service];
    }
}