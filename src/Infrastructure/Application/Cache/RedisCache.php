<?php

namespace Example\Infrastructure\Application\Cache;

use Example\Application\Cache\CacheInterface;

class RedisCache implements CacheInterface
{
    public function store($key, $value, $timeToLive = null)
    {
        // Use redis to store the value
    }

    /**
     * @param $key
     *
     * @return mixed|null Null if not present or expired
     */
    public function retrieve($key)
    {
        // Use redis to retrieve the value
    }

    public function remove($key)
    {
        // Delete from redis
    }
}