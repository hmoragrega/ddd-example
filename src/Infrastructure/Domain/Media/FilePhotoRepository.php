<?php

namespace Example\Infrastructure\Domain\Media;

use Example\Domain\Core\Collection;
use Example\Domain\Core\Exception\ConnectionFailureException;
use Example\Domain\Core\Id;
use Example\Domain\Media\Photo;
use Example\Domain\Media\PhotoRepositoryInterface;
use Example\Domain\Media\PhotoFactory;
use Example\Domain\User\User;
use Example\Infrastructure\Domain\Core\JsonFileRepositoryTrait;

/**
 * Class FilePhotoRepository
 *
 * @package Example\Infrastructure\Domain\Media
 */
class FilePhotoRepository implements PhotoRepositoryInterface
{
    use JsonFileRepositoryTrait;

    /**
     * @var mixed
     */
    private $photos;
    /**
     * @var PhotoFactory
     */
    private $factory;
    /**
     * @var \SplFileObject
     */
    private $photosFile;

    /**
     * FilePhotoRepository constructor.
     *
     * @param \SplFileObject $photosFile
     * @param PhotoFactory   $factory
     */
    public function __construct(\SplFileObject $photosFile, PhotoFactory $factory)
    {
        $this->photosFile = $photosFile;
        $this->factory = $factory;
        $this->photos = $this->loadFileContents($photosFile);
    }

    /**
     * @param User $user
     *
     * @return Collection
     */
    public function getAllByUser(User $user)
    {
        // Have fun debugging this!
        if (rand(1, 10) == 1) {
            throw new ConnectionFailureException("Memory is crumbling!");
        }

        $collection = new Collection();
        $userId = (string) $user->getId();

        foreach ($this->photos as $photo) {
            if ($photo['userId'] == $userId) {
                $collection[$photo['photoId']] = $this->factory->build(
                    (int) $photo['photoId'],
                    $photo['title'],
                    (int) $photo['userId'],
                    $photo['public'] == 'yes' ? true : false // Another approach
                );
            }
        }

        return $collection;
    }

    /**
     * @param Photo $photo
     *
     * @throws \Example\Infrastructure\Domain\Core\CriticalException
     */
    public function save(Photo $photo)
    {
        if ($photo->isNew()) {
            $photo->getId()->change(new Id(count($this->photos) + 1));
        }

        $this->photos[(string) $photo->getId()] = [
            'photoId' => (string) $photo->getId(),
            'title'   => (string) $photo->getTitle(),
            'userId'  => (string) $photo->getUserId(),
            'public'  => $photo->isPublic() ? 'yes' : 'no',
        ];

        $this->writeToFile($this->photosFile, $this->photos);
    }

    /**
     * @param User $user
     *
     * @throws \Example\Infrastructure\Domain\Core\CriticalException
     */
    public function deleteByUser(User $user)
    {
        // Have fun debugging this!
        if (rand(1, 10) == 1) {
            throw new ConnectionFailureException("Memory is crumbling!");
        }

        foreach ($this->photos as $photoId => $photo) {
            if ($photo['userId'] == $user->getId()) {
                unset($this->photos[$photoId]);
            }
        }

        $this->writeToFile($this->photosFile, $this->photos);
    }
}
