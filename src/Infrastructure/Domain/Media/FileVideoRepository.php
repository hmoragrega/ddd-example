<?php

namespace Example\Infrastructure\Domain\Media;

use Example\Domain\Core\Collection;
use Example\Domain\Media\Video;
use Example\Domain\Media\VideoFactory;
use Example\Domain\Media\VideoRepositoryInterface;
use Example\Domain\User\User;
use Example\Infrastructure\Domain\Core\JsonFileRepositoryTrait;

/**
 * Class FileVideoRepository
 *
 * @package Example\Infrastructure\Domain\Media
 */
class FileVideoRepository implements VideoRepositoryInterface
{
    use JsonFileRepositoryTrait;

    /**
     * @var mixed
     */
    private $videos;
    /**
     * @var VideoFactory
     */
    private $factory;
    /**
     * @var \SplFileObject
     */
    private $videosFile;

    /**
     * FileVideoRepository constructor.
     *
     * @param \SplFileObject $videosFile
     * @param VideoFactory   $factory
     */
    public function __construct(\SplFileObject $videosFile, VideoFactory $factory)
    {
        $this->videosFile = $videosFile;
        $this->factory = $factory;
        $this->videos = $this->loadFileContents($videosFile);
    }

    /**
     * @param User $user
     *
     * @return Collection
     */
    public function getAllByUser(User $user)
    {
        $collection = new Collection();
        $userId = (string) $user->getId();

        foreach ($this->videos as $video) {
            if ($video['userId'] == $userId) {
                $collection[$video['videoId']] = $this->factory->build(
                    (int) $video['videoId'],
                    $video['title'],
                    (int) $video['userId']
                );
            }
        }

        return $collection;
    }

    /**
     * @param Video $video
     *
     * @throws \Example\Infrastructure\Domain\Core\CriticalException
     */
    public function save(Video $video)
    {
        if ($video->getId()->isNew()) {
            $video->changeId(new Id(count($this->videos) + 1));
        }

        $this->videos[(string) $video->getId()] = [
            'photoId' => (string) $video->getId(),
            'title'   => (string) $video->getTitle(),
            'userId'  => (string) $video->getUserId(),
        ];

        $this->writeToFile($this->videosFile, $this->videos);
    }

    /**
     * @param User $user
     *
     * @throws \Example\Infrastructure\Domain\Core\CriticalException
     */
    public function deleteByUser(User $user)
    {
        foreach ($this->videos as $videoId => $video) {
            if ($video['userId'] == $user->getId()) {
                unset($this->videos[$videoId]);
            }
        }

        $this->writeToFile($this->videosFile, $this->videos);
    }
}