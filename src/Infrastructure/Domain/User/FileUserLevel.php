<?php

namespace Example\Infrastructure\Domain\User;

use Example\Domain\Core\Exception\ErrorException;
use Example\Domain\User\UserLevel;

/**
 * Class FileUserLevel
 *
 * @package Example\Infrastructure\Domain\User
 */
class FileUserLevel extends UserLevel
{
    private static $userLevelToFile = [
        UserLevel::FREE  => 1,
        UserLevel::ADMIN => 3,
    ];

    private static $fileToUserLevel = [
        1 => UserLevel::FREE,
        2 => UserLevel::FREE,
        3 => UserLevel::ADMIN,
    ];

    /**
     * @var int
     */
    private $legacyLevel;

    /**
     * FileUserLevel constructor.
     *
     * @param $level
     */
    public function __construct($level)
    {
        if (isset(self::$fileToUserLevel[$level])) {
            $this->legacyLevel = $level;
            $level = self::$fileToUserLevel[$level];
        } elseif (isset(self::$userLevelToFile[$level])) {
            $this->legacyLevel = self::$userLevelToFile[$level];
        }

        parent::__construct($level);
    }

    /**
     * @return int
     */
    public function getLegacyValue()
    {
        return $this->legacyLevel;
    }

    /**
     * @param UserLevel $userLevel
     *
     * @return mixed
     */
    public static function mapUserLevelToFileValue(UserLevel $userLevel)
    {
        if (!isset(self::$userLevelToFile[$userLevel->getValue()])) {
            throw new ErrorException("User level cannot be mapped to file");
        }

        return self::$userLevelToFile[$userLevel->getValue()];
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public static function mapFileValueToUserLevel($value)
    {
        if (!isset(self::$fileToUserLevel[$value])) {
            throw new ErrorException("File value cannot be mapped to a user level");
        }

        return self::$fileToUserLevel[$value];
    }
}