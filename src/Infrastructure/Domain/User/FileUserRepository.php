<?php

namespace Example\Infrastructure\Domain\User;

use Example\Domain\Core\Exception\CriticalException;
use Example\Domain\Core\Exception\EntityNotFoundException;
use Example\Domain\Core\Id;
use Example\Domain\User\User;
use Example\Domain\User\UserFactory;
use Example\Domain\User\UserName;
use Example\Domain\User\UserRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class FileUserRepository
 *
 * @package Example\Infrastructure\Domain\User
 */
class FileUserRepository implements UserRepositoryInterface
{
    /**
     * @var mixed
     */
    private $users;
    /**
     * @var UserFactory
     */
    private $factory;
    /**
     * @var \SplFileObject
     */
    private $usersFile;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * FileUserRepository constructor.
     *
     * @param \SplFileObject              $usersFile
     * @param UserFactory|FileUserFactory $factory
     * @param LoggerInterface             $logger
     */
    public function __construct(
        \SplFileObject $usersFile,
        UserFactory $factory,
        LoggerInterface $logger
    ) {
        $this->usersFile = $usersFile;
        $this->factory = $factory;
        $this->users = json_decode($usersFile->fread($usersFile->getSize()), true);
        $this->logger = $logger;
    }

    /**
     * @param Id $id
     *
     * @return User
     */
    public function getById(Id $id)
    {
        if (!isset($this->users[(string) $id])) {
            throw new EntityNotFoundException('User not found');
        }

        return $this->build($this->users[(string) $id]);
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        if ($user->isNew()) {
            $user->setId(new Id(count($this->users) + 1));
        }

        $this->writeUser($user);
    }

    /**
     * Updates the changed values of a use and updating the last update time
     * 
     * @param User $new
     * @param User $old
     */
    public function update(User $new, User $old)
    {
        $changes = [];

        if ($new->getName() != $old->getName()) {
            $changes[] = "Name ".$old->getName()." -> ".$new->getName();
        }

        if ($new->getLevel() != $old->getLevel()) {
            $changes[] = "Name ".$old->getLevel()." -> ".$new->getLevel();
        }

        // etc...

        if (empty($changes)) {
            return;
        }

        foreach ($changes as $change) {
            $this->logger->debug($change);
        }

        $this->writeUser($new);
    }

    /**
     * @param User $user
     */
    protected function writeUser(User $user)
    {
        // Map preserving the legacy value
        $level = $user->getLevel();
        $level = $level instanceof FileUserLevel
            ? $level->getLegacyValue()
            : FileUserLevel::mapUserLevelToFileValue($level);

        $user->setLastUpdate();

        $this->users[(string) $user->getId()] = [
            'userId'     => (string) $user->getId(),
            'name'       => (string) $user->getName(),
            'level'      => $level,
            'photoCount' => $user->getPhotoCounter()->getValue(),
            'videoCount' => $user->getVideoCounter()->getValue(),
            'lastUpdate' => $user->getLastUpdate()->getTimeStamp(),
        ];

        $this->writeToFile();
    }

    /**
     * @param User $user
     */
    public function delete(User $user)
    {
        unset($this->users[(string) $user->getId()]);
        $this->writeToFile();
    }

    /**
     * @param array $data
     *
     * @return User
     */
    private function build(array $data)
    {
        return $this->factory->build(
            (int) $data['userId'],
            $data['name'],
            $data['level'],
            $data['photoCount'],
            $data['videoCount'],
            $data['lastUpdate']
        );
    }

    /**
     * @param UserName $name
     *
     * @return bool
     */
    public function nameExists(UserName $name)
    {
        foreach ($this->users as $user) {
            if ($user['name'] == $name->getString()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Writes the users to the file
     */
    private function writeToFile()
    {
        if (!$this->usersFile->ftruncate(0)) {
            throw new CriticalException("Could not truncate the users file");
        }

        $this->usersFile->fseek(0);
        if (!$this->usersFile->fwrite(trim(json_encode($this->users, JSON_PRETTY_PRINT)))) {
            throw new CriticalException("Error writing the users in the file");
        }

        $this->logger->debug("Users file written");
    }
}
