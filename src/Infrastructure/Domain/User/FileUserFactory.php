<?php

namespace Example\Infrastructure\Domain\User;

use Example\Domain\User\UserFactory;

class FileUserFactory extends UserFactory
{
    /**
     * @param $legacyLevel
     *
     * @return $this
     */
    public function addLevel($legacyLevel)
    {
        $this->constraints[self::LEVEL] = function () use ($legacyLevel) {
            return new FileUserLevel($legacyLevel);
        };

        return $this;
    }
}
