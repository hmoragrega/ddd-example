<?php

namespace Example\Infrastructure\Domain\Core;

use Example\Domain\Core\ParametersInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class SymfonyParametersAdapter
 *
 * @package Example\Infrastructure\Domain\Core
 */
class SymfonyParametersAdapter implements ParametersInterface
{
    /**
     * @var ParameterBag
     */
    private $parameters;

    /**
     * SymfonyParametersAdapter constructor.
     *
     * @param ParameterBag $parameters
     */
    public function __construct(ParameterBag $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return $this->parameters->get($key, $default);
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        return $this->parameters->set($key, $value);
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function has($key)
    {
        return $this->parameters->has($key);
    }

    /**
     * @param      $key
     *
     * @return mixed
     */
    public function demand($key)
    {
        if (!$this->has($key)) {
            throw new \OutOfBoundsException("The key $key is not in the parameters");
        }

        return $this->get($key);
    }
}