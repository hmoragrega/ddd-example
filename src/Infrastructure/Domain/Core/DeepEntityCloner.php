<?php

namespace Example\Infrastructure\Domain\Core;

use DeepCopy\DeepCopy;
use Example\Domain\Core\EntityClonerInterface;
use Example\Domain\Core\EntityInterface;

/**
 * Class DeepEntityCloner
 *
 * @package Example\Infrastructure\Domain\Core
 */
class DeepEntityCloner implements EntityClonerInterface
{
    /**
     * @var DeepCopy
     */
    private $deepCopy;

    /**
     * DeepCopyEntityManager constructor.
     *
     * @param DeepCopy        $deepCopy
     */
    public function __construct(DeepCopy $deepCopy)
    {
        $this->deepCopy = $deepCopy;
    }

    /**
     * Gets a deep copy of the entity
     *
     * @param EntityInterface $entity
     *
     * @return EntityInterface $entity
     */
    public function copy(EntityInterface $entity)
    {
        return $this->deepCopy->copy($entity);
    }
}
