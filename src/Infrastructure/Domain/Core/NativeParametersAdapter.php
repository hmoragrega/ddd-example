<?php

namespace Example\Infrastructure\Domain\Core;

use Example\Domain\Core\ParametersInterface;

/**
 * Class NativeParametersAdapter
 *
 * @package Example\Infrastructure\Domain\Core
 */
class NativeParametersAdapter implements ParametersInterface
{
    /**
     * @var array
     */
    private $parameters;

    /**
     * NativeParametersAdapter constructor.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return null
     */
    public function get($key, $default = null)
    {
        if (!isset($this->parameters[$key])) {
            return $default;
        }
        
        return $this->parameters[$key];
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $this->parameters[$key] = $value;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function has($key)
    {
        return isset($key, $this->parameters);
    }

    /**
     * @param      $key
     *
     * @return mixed
     */
    public function demand($key)
    {
        if (!$this->has($key)) {
            throw new \OutOfBoundsException("The key $key is not in the parameters");
        }

        return $this->get($key);
    }
}