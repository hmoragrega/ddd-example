<?php

namespace Example\Infrastructure\Domain\Core;

trait JsonFileRepositoryTrait
{
    public function loadFileContents(\SplFileObject $file)
    {
        return json_decode($file->fread($file->getSize()), true);
    }

    private function writeToFile(\SplFileObject $file, array $data)
    {
        if (!$file->ftruncate(0)) {
            throw new CriticalException("Could not truncate the file");
        }

        $file->fseek(0);
        if (!$file->fwrite(trim(json_encode($data, JSON_PRETTY_PRINT)))) {
            throw new CriticalException("Error writing the file");
        }
    }
}