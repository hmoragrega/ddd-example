<?php

namespace Example\Infrastructure\Domain\Core\Events;

use Example\Domain\Core\Transactional\TransactionObserverInterface;
use Example\Domain\Core\Events\AbstractDomainEvent;
use Example\Domain\Core\Events\DispatcherInterface;
use Example\Domain\Core\Events\PublisherInterface;
use Example\Domain\Core\Events\String;
use Psr\Log\LoggerInterface;

/**
 * Class SynchronousEventService
 *
 * @package Example\Infrastructure\Domain\Core\Events
 */
class SynchronousEventService implements PublisherInterface, DispatcherInterface, TransactionObserverInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var array
     */
    private $subscribers = [];

    /**
     * @var bool
     */
    private $inTransaction = false;
    /**
     * @var array
     */
    private $pendingEvents = [];

    /**
     * SynchronousEventService constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param AbstractDomainEvent $domainEvent
     */
    public function publish(AbstractDomainEvent $domainEvent)
    {
        $this->logger->debug("New event ".get_class($domainEvent)." fired");

        if ($this->inTransaction) {
            $this->logger->debug("Waiting for transaction to finish before dispatching the event");
            $this->pendingEvents[] = $domainEvent;

            return;
        }

        $this->dispatchEventToAllSubscribers($domainEvent);
    }

    /**
     * @param AbstractDomainEvent $domainEvent
     */
    private function dispatchEventToAllSubscribers(AbstractDomainEvent $domainEvent)
    {
        if (!isset($this->subscribers[get_class($domainEvent)])) {
            $this->logger->debug("No subscribers for the event");

            return;
        }

        $this->logger->debug("Dispatching to subscribers");

        foreach ($this->subscribers[get_class($domainEvent)] as $subscriber) {
            $this->dispatchEventToHandler($domainEvent, $subscriber);
        }
    }

    /**
     * @param          $domainEvent
     * @param callable $handler
     */
    private function dispatchEventToHandler($domainEvent, callable $handler)
    {
        try {
            call_user_func($handler, $domainEvent);
        } catch (\Exception $exception) {
            $this->logger->error(
                "Exception handling event: ".$exception->getMessage(),
                [
                    'event'     => $domainEvent,
                    'exception' => $exception,
                ]
            );
        }
    }

    /**
     * @param          $eventClass
     * @param callable $handler
     */
    public function subscribe($eventClass, callable $handler)
    {
        $this->subscribers[$eventClass][] = $handler;
    }

    /**
     * @param array    $eventClasses
     * @param callable $handler
     */
    public function subscribeAll(array $eventClasses, callable $handler)
    {
        foreach ($eventClasses as $eventClass) {
            $this->subscribe($eventClass, $handler);
        }
    }

    /**
     *
     */
    public function transactionHasStarted()
    {
        $this->inTransaction = true;
        $this->pendingEvents = [];
    }

    /**
     *
     */
    public function transactionHasBeenCommitted()
    {
        $this->logger->debug("Dispatching ".count($this->pendingEvents)." transaction events");

        foreach ($this->pendingEvents as $event) {
            $this->logger->debug("Dispatching Pending event ".get_class($event));
            $this->dispatchEventToAllSubscribers($event);
        }

        $this->inTransaction = false;
        $this->pendingEvents = [];
    }

    /**
     *
     */
    public function transactionHasBeenRolledBack()
    {
        $this->logger->debug('Discarding events generated in a transaction');
        $this->inTransaction = false;
        $this->pendingEvents = [];
    }
}