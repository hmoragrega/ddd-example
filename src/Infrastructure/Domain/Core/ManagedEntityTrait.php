<?php

namespace Example\Infrastructure\Domain\Core;

trait ManagedEntityTrait
{
    protected $restore = [];

    protected function trackChange($property, callable $restore)
    {
        if (!array_key_exists($property, $this->restore)) {
            $this->restore[$property] = $restore;
        };
    }

    public function getChanges()
    {
        return array_keys($this->restore);
    }

    protected function flushChanges()
    {
        $this->restore = [];
    }

    protected function restore()
    {
        foreach ($this->restore as $restore) {
            call_user_func($restore);
        }
    }
}