<?php

namespace Example\Infrastructure\Domain\Core;

use Example\Domain\Core\ParametersInterface;
use Zend\Stdlib\Parameters;

/**
 * Class ZendParametersAdapter
 *
 * @package Example\Infrastructure\Domain\Core
 */
class ZendParametersAdapter implements ParametersInterface
{
    /**
     * @var Parameters
     */
    private $parameters;

    /**
     * ZendParametersAdapter constructor.
     *
     * @param Parameters $parameters
     */
    public function __construct(Parameters $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return $this->parameters->get($key, $default);
    }

    /**
     * @param $key
     * @param $value
     *
     * @return Parameters
     */
    public function set($key, $value)
    {
        return $this->parameters->set($key, $value);
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function has($key)
    {
        return $this->parameters->has($key);
    }

    /**
     * @param      $key
     *
     * @return mixed
     */
    public function demand($key)
    {
        if (!$this->has($key)) {
            throw new \OutOfBoundsException("The key $key is not in the parameters");
        }

        return $this->get($key);
    }
}
