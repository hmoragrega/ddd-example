<?php

namespace Example\Infrastructure\Domain\Core\Transactional;

use Example\Domain\Core\Transactional\TransactionalSystemInterface;
use Psr\Log\LoggerInterface;

class FileTransactionalSystem implements TransactionalSystemInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * FileTransactionalSystem constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function startTransaction()
    {
        $this->logger->debug('Start transaction');
    }

    public function commit()
    {
        $this->logger->debug('Commit transaction');
    }

    public function rollback()
    {
        $this->logger->debug('Rollback transaction');
    }
}