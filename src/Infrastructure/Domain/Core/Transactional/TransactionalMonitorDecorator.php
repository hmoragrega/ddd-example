<?php

namespace Example\Infrastructure\Domain\Transactional;

use Example\Application\Monitoring\MonitorInterface;
use Example\Domain\Core\NonEmptyString;
use Example\Domain\Core\Transactional\TransactionObserverInterface;

class TransactionalMonitorDecorator implements TransactionObserverInterface
{
    /**
     * @var MonitorInterface
     */
    private $monitor;

    /**
     * @var string
     */
    private $metric;

    /**
     * TransactionalMonitorDecorator constructor.
     *
     * @param MonitorInterface $monitor
     * @param NonEmptyString   $metric
     */
    public function __construct(MonitorInterface $monitor, NonEmptyString $metric)
    {
        $this->monitor = $monitor;
        $this->metric  = (string) $metric;
    }

    /**
     * It gets notified that a transaction has started
     */
    public function transactionHasStarted()
    {
        $this->monitor->start($this->metric);
    }

    /**
     * Callback that gets executed when a transaction has been committed
     */
    public function transactionHasBeenCommitted()
    {
        $this->monitor->end($this->metric, ['result' => 'commit']);
    }

    /**
     * Callback tht gets executed when a transaction has been rolled back
     */
    public function transactionHasBeenRolledBack()
    {
        $this->monitor->end($this->metric, ['result' => 'rollback']);
    }
}
