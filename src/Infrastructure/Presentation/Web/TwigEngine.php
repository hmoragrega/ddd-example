<?php

namespace Example\Infrastructure\Presentation\Web;

use Example\Presentation\Web\TemplateEngineInterface;

class TwigEngine implements TemplateEngineInterface
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function render($template, $variables = [])
    {
        return $this->twig->render($template, $variables);
    }
}