<?php

namespace Example\Domain\Media\Command;

use Example\Domain\Core\Command\AbstractTransactionalCommand;
use Example\Domain\Core\Command\PublisherCommandInterface;
use Example\Domain\Core\Command\PublisherCommandTrait;
use Example\Domain\Core\Command\TransactionalCommandInterface;
use Example\Domain\Core\Events\PublisherAwareInterface;
use Example\Domain\Core\Events\PublisherAwareTrait;
use Example\Domain\Core\ParametersInterface;
use Example\Domain\Core\Transactional\TransactionalAwareTrait;
use Example\Domain\Core\Transactional\TransactionalExecutorTrait;
use Example\Domain\Media\Events\PhotoCreatedEvent;
use Example\Domain\Media\PhotoFactory;
use Example\Domain\Media\PhotoRepositoryInterface;
use Example\Domain\User\UserRepositoryInterface;

/**
 * Class PhotoCreator
 *
 * @package Example\Domain\Media\Command
 */
class CreatePhotoCommand extends AbstractTransactionalCommand implements PublisherAwareInterface
{
    use TransactionalAwareTrait, PublisherAwareTrait, TransactionalExecutorTrait;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var PhotoRepositoryInterface
     */
    private $photoRepository;

    /**
     * @var PhotoFactory
     */
    private $photoFactory;

    /**
     * PhotoCreator constructor.
     *
     * @param UserRepositoryInterface  $userRepository
     * @param PhotoRepositoryInterface $photoRepository
     * @param PhotoFactory             $photoFactory
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        PhotoRepositoryInterface $photoRepository,
        PhotoFactory $photoFactory
    ) {
        $this->userRepository = $userRepository;
        $this->photoRepository = $photoRepository;
        $this->photoFactory = $photoFactory;
    }

    /**
     * @param ParametersInterface $parameters
     *
     * @return mixed
     * @throws \Exception
     */
    public function execute(ParametersInterface $parameters)
    {
        return $this->executeTransaction(
            function () use ($parameters) {
                $photo = $this->photoFactory->build(
                    null,
                    $parameters->get('title'),
                    $parameters->get('userId'),
                    $parameters->get('isPublic')
                );

                // Check that the user exists
                $user = $this->userRepository->getById($photo->getUserId());

                // Save the photo
                $this->photoRepository->save($photo);

                // Increment the photo counter
                $user->getPhotoCounter()->increment();

                // Persist the user
                $this->userRepository->persist($user);

                // Publish the event
                $this->getPublisher()->publish(new PhotoCreatedEvent($photo, $user));

                return $photo;
            }
        );
    }
}
