<?php

namespace Example\Domain\Media\Command;

use Example\Application\UseCaseInterface;
use Example\Domain\Core\Command\CommandInterface;
use Example\Domain\Core\Id;
use Example\Domain\Core\ParametersInterface;
use Example\Domain\Media\PhotoRepositoryInterface;
use Example\Domain\User\UserRepositoryInterface;

/**
 * Class GetPhotosByUserUseCase
 *
 * @package Example\Application\Media
 */
class GetPhotosByUserCommand implements CommandInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var PhotoRepositoryInterface
     */
    private $photoRepository;

    /**
     * GetPhotosByUserCommand constructor.
     *
     * @param UserRepositoryInterface  $userRepository
     * @param PhotoRepositoryInterface $photoRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        PhotoRepositoryInterface $photoRepository
    ) {
        $this->userRepository = $userRepository;
        $this->photoRepository = $photoRepository;
    }

    /**
     * @param ParametersInterface $parameters
     *
     * @return \Example\Domain\Core\Collection
     */
    public function execute(ParametersInterface $parameters)
    {
        $userId = new Id($parameters->get('userId'));
        $user = $this->userRepository->getById($userId);

        return $this->photoRepository->getAllByUser($user);
    }
}
