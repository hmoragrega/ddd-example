<?php

namespace Example\Domain\Media;

use Example\Domain\Core\Id;
use Example\Domain\Core\Validator;

class VideoFactory extends Validator
{
    const VIDEO_ID = 'videoId';
    const TITLE = 'title';
    const USER_ID = 'userId';

    public function build($videoId, $title, $userId)
    {
        $this->addId($videoId);
        $this->addTitle($title);
        $this->addUserId($userId);

        extract($this->validate());

        return new Video($videoId, $title, $userId);
    }

    /**
     * @param $videoId
     */
    public function addId($videoId)
    {
        $this->addConstraint(
            self::VIDEO_ID,
            function () use ($videoId) {
                return new Id($videoId, 'video id');
            }
        );
    }

    /**
     * @param $title
     */
    public function addTitle($title)
    {
        $this->addConstraint(
            self::TITLE,
            function () use ($title) {
                return new MediaTitle($title, 'video title');
            }
        );
    }

    /**
     * @param $userId
     */
    public function addUserId($userId)
    {
        $this->addConstraint(
            self::USER_ID,
            function () use ($userId) {
                return new Id($userId, 'user id');
            }
        );
    }
}
