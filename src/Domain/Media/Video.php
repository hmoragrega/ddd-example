<?php

namespace Example\Domain\Media;

use Example\Domain\Core\Chars;
use Example\Domain\Core\ComparableInterface;
use Example\Domain\Core\EntityInterface;
use Example\Domain\Core\EntityTrait;
use Example\Domain\Core\Id;
use Example\Domain\Core\String;

/**
 * Class Video
 *
 * @package Example\Domain\Media
 */
class Video implements EntityInterface
{
    use EntityTrait;

    /**
     * @var MediaTitle
     */
    private $title;

    /**
     * @var Id
     */
    private $userId;

    /**
     * Video constructor.
     *
     * @param Id         $id
     * @param MediaTitle $title
     * @param Id         $userId
     */
    public function __construct(Id $id, MediaTitle $title, Id $userId)
    {
        $this->id = $id;
        $this->title = $title;
        $this->userId = $userId;
    }

    /**
     * @return MediaTitle
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Id
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'videoId' => $this->getId()->getValue(),
            'title'   => (string) $this->getTitle(),
            'userId'  => $this->getUserId()->getValue(),
        ];
    }
}