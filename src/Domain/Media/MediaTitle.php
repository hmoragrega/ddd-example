<?php

namespace Example\Domain\Media;

use Example\Domain\Core\ValueObject\StringBetween;

class MediaTitle extends StringBetween
{
    private static $min = 0;
    private static $max = 100;

    public function __construct($title, $name = 'media title')
    {
        parent::__construct($title, static::$min, static::$max, $name);
    }
}
