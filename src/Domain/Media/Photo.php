<?php

namespace Example\Domain\Media;

use Example\Domain\Core\ComparableInterface;
use Example\Domain\Core\EntityInterface;
use Example\Domain\Core\EntityTrait;
use Example\Domain\Core\Id;
use Example\Domain\Core\ValueObject\Boolean;

/**
 * Class Photo
 *
 * @package Example\Domain\Media
 */
class Photo implements EntityInterface
{
    use EntityTrait;

    /**
     * @var MediaTitle
     */
    private $title;

    /**
     * @var
     */
    private $userId;

    /**
     * @var Boolean
     */
    private $isPublic;

    /**
     * Photo constructor.
     *
     * @param Id         $id
     * @param MediaTitle $title
     * @param Id         $userId
     * @param            $isPublic
     */
    public function __construct(Id $id, MediaTitle $title, Id $userId, Boolean $isPublic)
    {
        $this->id         = $id;
        $this->title      = $title;
        $this->userId     = $userId;
        $this->isPublic   = $isPublic;
    }

    /**
     * @return MediaTitle
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Id
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return bool
     */
    public function isPublic()
    {
        return $this->isPublic->getValue();
    }

    /**
     * @return bool
     */
    public function isPrivate()
    {
        return !$this->isPublic();
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'photoId' => $this->getId()->getValue(),
            'title'   => (string) $this->getTitle(),
            'userId'  => $this->getUserId()->getValue(),
            'isPublic'=> $this->isPublic(),
        ];
    }
}