<?php

namespace Example\Domain\Media\Events;

use Example\Domain\Core\Events\AbstractDomainEvent;
use Example\Domain\Media\Photo;
use Example\Domain\User\User;

class AbstractPhotoEvent extends AbstractDomainEvent
{
    private $photo;
    private $user;

    public function __construct(Photo $photo, User $user)
    {
        parent::__construct();
        $this->photo = $photo;
        $this->user = $user;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function getUser()
    {
        return $this->user;
    }

    function jsonSerialize()
    {
        return [
            'photo'     => $this->photo,
            'user'      => $this->user,
            'ocurredOn' => $this->getTime()->getTimestamp(),
        ];
    }
}
