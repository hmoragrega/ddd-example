<?php

namespace Example\Domain\Media;

use Example\Domain\Core\Collection;
use Example\Domain\User\User;

interface PhotoRepositoryInterface
{
    /**
     * @param User $user
     *
     * @return Collection
     */
    public function getAllByUser(User $user);

    public function save(Photo $photo);

    public function deleteByUser(User $user);
}