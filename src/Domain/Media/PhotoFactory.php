<?php

namespace Example\Domain\Media;

use Example\Domain\Core\Id;
use Example\Domain\Core\Validator;
use Example\Domain\Core\ValueObject\Boolean;

class PhotoFactory extends Validator
{
    const PHOTO_ID = 'photoId';
    const TITLE = 'title';
    const USER_ID = 'userId';
    const IS_PUBLIC = 'isPublic';

    
    public function build($photoId, $title, $userId, $isPublic)
    {
        $this->addId($photoId);
        $this->addTitle($title);
        $this->addUserId($userId);
        $this->addIsPublic($isPublic);

        extract($this->validate());

        return new Photo($photoId, $title, $userId, $isPublic);
    }

    /**
     * @param $photoId
     */
    public function addId($photoId)
    {
        $this->addConstraint(
            self::PHOTO_ID,
            function () use ($photoId) {
                return new Id($photoId, 'photo id');
            }
        );
    }

    /**
     * @param $title
     */
    public function addTitle($title)
    {
        $this->addConstraint(
            self::TITLE,
            function () use ($title) {
                return new MediaTitle($title, 'photo title');
            }
        );
    }

    /**
     * @param $userId
     */
    public function addUserId($userId)
    {
        $this->addConstraint(
            self::USER_ID,
            function () use ($userId) {
                return new Id($userId, 'user id');
            }
        );
    }

    /**
     * @param $isPublic
     */
    public function addIsPublic($isPublic)
    {
        $this->addConstraint(
            self::IS_PUBLIC,
            function () use ($isPublic) {
                return new Boolean($isPublic, '"is public"');
            }
        );
    }
}
