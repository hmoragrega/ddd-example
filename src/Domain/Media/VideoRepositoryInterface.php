<?php

namespace Example\Domain\Media;

use Example\Domain\Core\Collection;
use Example\Domain\User\User;

interface VideoRepositoryInterface
{
    /**
     * @param User $user
     *
     * @return Collection
     */
    public function getAllByUser(User $user);

    public function save(Video $video);

    public function deleteByUser(User $user);
}