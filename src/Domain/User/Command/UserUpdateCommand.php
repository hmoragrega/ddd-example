<?php

namespace Example\Domain\User\Command;

use Example\Domain\Core\Command\CommandInterface;
use Example\Domain\Core\EntityClonerInterface;
use Example\Domain\Core\EntityRegistryInterface;
use Example\Domain\Core\Events\PublisherAwareInterface;
use Example\Domain\Core\Events\PublisherAwareTrait;
use Example\Domain\Core\Id;
use Example\Domain\Core\ParametersInterface;
use Example\Domain\User\Events\UserUpdatedEvent;
use Example\Domain\User\UserFactory;
use Example\Domain\User\UserRepositoryInterface;
use Example\Domain\User\UserValidator;

/**
 * Class OnDuplicateUserCommand
 *
 * @package Example\Domain\User\Command
 */
class UserUpdateCommand implements CommandInterface, PublisherAwareInterface
{
    use PublisherAwareTrait;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var UserFactory
     */
    private $userFactory;
    /**
     * @var EntityClonerInterface
     */
    private $cloner;

    /**
     * UserUpdateCommand constructor.
     *
     * @param EntityClonerInterface   $cloner
     * @param UserRepositoryInterface $userRepository
     * @param UserFactory             $userFactory
     */
    public function __construct(
        EntityClonerInterface $cloner,
        UserRepositoryInterface $userRepository,
        UserFactory $userFactory
    ) {
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
        $this->cloner = $cloner;
    }

    /**
     * @param ParametersInterface $params
     *
     * @return \Example\Domain\User\User
     */
    public function execute(ParametersInterface $params)
    {
        $userId = $params->demand('userId');
        $user = $this->userRepository->getById(new Id($userId));

        $this->userFactory->reset();
        if ($params->has('name')) {
            $this->userFactory->addName($params->get('name'));
        }
        if ($params->has('level')) {
            $this->userFactory->addLevel($params->get('level'));
        }

        // Validate and update
        $old = $this->cloner->copy($user);
        $this->userFactory->update($user);

        // If has changed persists the entity and notify with an event
        if ($user != $old) {
            $this->userRepository->update($user, $old);
            $this->getPublisher()->publish(new UserUpdatedEvent($user, $old));
        }

        return $user;
    }
}
