<?php

namespace Example\Domain\User\Command;

use Example\Domain\Core\Exception\DuplicatedEntityException;
use Example\Domain\Core\ParametersInterface;

/**
 * Class OnDuplicateUserCommand
 *
 * @package Example\Domain\User\Command
 */
class OnDuplicateUserCommand extends UserCreateCommand
{
    /**
     * @param ParametersInterface $params
     *
     * @return \Example\Domain\User\User
     */
    public function execute(ParametersInterface $params)
    {
        try {
            return parent::execute($params);
        } catch (DuplicatedEntityException $exception) {
            // Send to lead or whatever
        }
    }
}
