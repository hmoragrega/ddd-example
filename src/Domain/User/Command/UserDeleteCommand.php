<?php

namespace Example\Domain\User\Command;

use Example\Domain\Core\Command\AbstractTransactionalCommand;
use Example\Domain\Core\Command\TransactionalSystemInterface;
use Example\Domain\Core\Events\PublisherAwareInterface;
use Example\Domain\Core\Events\PublisherAwareTrait;
use Example\Domain\Core\Id;
use Example\Domain\Core\ParametersInterface;
use Example\Domain\Media\PhotoRepositoryInterface;
use Example\Domain\Media\VideoRepositoryInterface;
use Example\Domain\User\Events\UserDeletedEvent;
use Example\Domain\User\UserRepositoryInterface;

/**
 * Class UserDeleteCommand
 *
 * @package Example\Domain\User\Command
 */
class UserDeleteCommand extends AbstractTransactionalCommand implements PublisherAwareInterface
{
    use PublisherAwareTrait;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var PhotoRepositoryInterface
     */
    private $photoRepository;

    /**
     * @var VideoRepositoryInterface
     */
    private $videoRepository;

    /**
     * GetProfileUseCase constructor.
     *
     * @param UserRepositoryInterface      $userRepository
     * @param PhotoRepositoryInterface     $photoRepository
     * @param VideoRepositoryInterface     $videoRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        PhotoRepositoryInterface $photoRepository,
        VideoRepositoryInterface $videoRepository
    ) {
        $this->userRepository = $userRepository;
        $this->photoRepository = $photoRepository;
        $this->videoRepository = $videoRepository;
    }

    public function execute(ParametersInterface $parameters)
    {
        $user = $this->userRepository->getById(new Id($parameters->get('userId')));

        $this->executeTransaction(
            function () use ($user) {
                $this->userRepository->delete($user);
                $this->photoRepository->deleteByUser($user);
                $this->videoRepository->deleteByUser($user);
            }
        );

        $this->publisher->publish(new UserDeletedEvent($user));
    }
}
