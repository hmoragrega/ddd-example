<?php

namespace Example\Domain\User\Command;

use Example\Domain\Core\Command\CommandInterface;
use Example\Domain\Core\Id;
use Example\Domain\Core\ParametersInterface;
use Example\Domain\Media\PhotoRepositoryInterface;
use Example\Domain\Media\VideoRepositoryInterface;
use Example\Domain\User\UserRepositoryInterface;

class GetUserProfileCommand implements CommandInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var PhotoRepositoryInterface
     */
    private $photoRepository;

    /**
     * @var VideoRepositoryInterface
     */
    private $videoRepository;

    /**
     * GetProfileUseCase constructor.
     *
     * @param UserRepositoryInterface  $userRepository
     * @param PhotoRepositoryInterface $photoRepository
     * @param VideoRepositoryInterface $videoRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        PhotoRepositoryInterface $photoRepository,
        VideoRepositoryInterface $videoRepository
    ) {
        $this->userRepository = $userRepository;
        $this->photoRepository = $photoRepository;
        $this->videoRepository = $videoRepository;
    }

    /**
     * @param ParametersInterface $parameters
     *
     * @return GetProfileResponse
     */
    public function execute(ParametersInterface $parameters)
    {
        $userId = new Id($parameters->get('userId'));
        $user = $this->userRepository->getById($userId);
        $photos = $this->photoRepository->getAllByUser($user);
        $videos = $this->videoRepository->getAllByUser($user);

        return ['user' => $user, 'photos' => $photos, 'videos' => $videos];
    }
}
