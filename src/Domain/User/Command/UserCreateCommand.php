<?php

namespace Example\Domain\User\Command;

use Example\Domain\Core\Command\CommandInterface;
use Example\Domain\Core\Events\PublisherAwareInterface;
use Example\Domain\Core\Events\PublisherAwareTrait;
use Example\Domain\Core\Events\PublisherInterface;
use Example\Domain\Core\Exception\DuplicatedEntityException;
use Example\Domain\Core\ParametersInterface;
use Example\Domain\User\Events\UserCreatedEvent;
use Example\Domain\User\UserFactory;
use Example\Domain\User\UserRepositoryInterface;

/**
 * Class UserCreateCommand
 *
 * @package Example\Domain\User\Command
 */
class UserCreateCommand implements CommandInterface, PublisherAwareInterface
{
    use PublisherAwareTrait;

    const USER_NAME_ALREADY_EXISTS = "User name already exists";
    const DEFAULT_COUNTER = 0;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserCreateCommand constructor.
     *
     * @param UserFactory             $userFactory
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserFactory $userFactory, UserRepositoryInterface $userRepository)
    {
        $this->userFactory = $userFactory;
        $this->userRepository = $userRepository;
    }

    /**
     * @param ParametersInterface $params
     *
     * @return \Example\Domain\User\User
     */
    public function execute(ParametersInterface $params)
    {
        $user = $this->userFactory->build(
            null,
            $params->get('name'),
            $params->get('level'),
            self::DEFAULT_COUNTER,
            self::DEFAULT_COUNTER,
            time()
        );

        if ($this->userRepository->nameExists($user->getName())) {
            throw (new DuplicatedEntityException())->setErrors(['name' => self::USER_NAME_ALREADY_EXISTS]);
        }

        $this->userRepository->persist($user);
        $this->publisher->publish(new UserCreatedEvent($user));

        return $user;
    }
}
