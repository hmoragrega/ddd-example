<?php

namespace Example\Domain\User\Events;

use Example\Domain\User\User;

class UserUpdatedEvent extends AbstractUserEvent
{
    private $original;

    /**
     * AbstractUserEvent constructor.
     *
     * @param User $new
     * @param User $original
     */
    public function __construct(User $new, User $original)
    {
        parent::__construct($new);
        $this->original = $original;
    }

    /**
     * @return User
     */
    public function getOriginal()
    {
        return $this->original;
    }
}
