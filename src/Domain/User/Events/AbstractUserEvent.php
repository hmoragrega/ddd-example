<?php

namespace Example\Domain\User\Events;

use Example\Domain\Core\Events\AbstractDomainEvent;
use Example\Domain\User\User;

/**
 * Class AbstractUserEvent
 *
 * @package Example\Domain\User\Events
 */
class AbstractUserEvent extends AbstractDomainEvent
{
    /**
     * @var User
     */
    private $user;

    /**
     * AbstractUserEvent constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'user'      => $this->user,
            'ocurredOn' => $this->getTime()->getTimestamp(),
        ];
    }
}
