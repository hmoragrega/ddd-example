<?php

namespace Example\Domain\User;

use Example\Domain\Core\ValueObject\StringInValues;

/**
 * Class UserLevel
 *
 * @package Example\Domain\User
 */
class UserLevel extends StringInValues
{
    const FREE  = 'free';
    const ADMIN = 'admin';

    private static $validLevels = [self::FREE, self::ADMIN];

    /**
     * UserLevel constructor.
     *
     * @param $level
     */
    public function __construct($level)
    {
        parent::__construct($level, self::$validLevels, 'user level');
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->value == self::ADMIN;
    }

    /**
     * @return bool
     */
    public function isFree()
    {
        return $this->value == self::FREE;
    }

    /**
     * @return bool
     */
    public function convertToAdmin()
    {
        return $this->value = self::ADMIN;
    }

    /**
     * @return bool
     */
    public function convertToFree()
    {
        return $this->value = self::FREE;
    }

    /**
     * @return array
     */
    public static function getValidValues()
    {
        return self::$validLevels;
    }
}
