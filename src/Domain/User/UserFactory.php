<?php

namespace Example\Domain\User;

use Example\Domain\Core\EntityFactoryTrait;
use Example\Domain\Core\Id;
use Example\Domain\Core\ValueObject\IntegerPositiveOrZero;
use Example\Domain\Core\ValueObject\IntegerZeroOrPositive;
use Example\Domain\Core\ValueObject\TimeStamp;

/**
 * Class UserFactory
 *
 * @package Example\Domain\User
 */
class UserFactory
{
    use EntityFactoryTrait;

    const USER_ID = 'userId';
    const NAME = 'name';
    const LEVEL = 'level';
    const PHOTO_COUNT = 'photoCount';
    const VIDEO_COUNT = 'videoCount';
    const LAST_UPDATE = 'lastUpdate';

    /**
     * @param $userId
     * @param $name
     * @param $level
     * @param $photoCount
     * @param $videoCount
     * @param $lastUpdate
     *
     * @return User
     */
    public function build($userId, $name, $level, $photoCount, $videoCount, $lastUpdate)
    {
        $results = $this
            ->addId($userId)
            ->addName($name)
            ->addLevel($level)
            ->addPhotoCount($photoCount)
            ->addVideoCount($videoCount)
            ->addLastUpdate($lastUpdate)
            ->validate();

        extract($results);

        return new User($userId, $name, $level, $photoCount, $videoCount, $lastUpdate);
    }

    /**
     * @param $userId
     *
     * @return $this
     */
    public function addId($userId)
    {
        $this->constraints[self::USER_ID] = function () use ($userId) {
            return new Id($userId);
        };

        return $this;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function addName($name)
    {
        $this->constraints[self::NAME] = function () use ($name) {
            return new UserName($name);
        };

        return $this;
    }

    /**
     * @param $level
     *
     * @return $this
     */
    public function addLevel($level)
    {
        $this->constraints[self::LEVEL] = function () use ($level) {
            return new UserLevel($level);
        };

        return $this;
    }

    /**
     * @param $photoCount
     *
     * @return $this
     */
    public function addPhotoCount($photoCount)
    {
        $this->constraints[self::PHOTO_COUNT] = function () use ($photoCount) {
            return new IntegerPositiveOrZero($photoCount);
        };

        return $this;
    }

    /**
     * @param $videoCount
     *
     * @return $this
     */
    public function addVideoCount($videoCount)
    {
        $this->constraints[self::VIDEO_COUNT] = function () use ($videoCount) {
            return new IntegerPositiveOrZero($videoCount);
        };

        return $this;
    }

    /**
     * @param $lastUpdate
     *
     * @return $this
     */
    public function addLastUpdate($lastUpdate)
    {
        $this->constraints[self::LAST_UPDATE] = function () use ($lastUpdate) {
            return new TimeStamp($lastUpdate, 'last update');
        };

        return $this;
    }

    /**
     * Updates the user with the new validated changes
     *
     * @param $user
     *
     * @return User
     */
    public function update(User $user)
    {
        foreach ($this->validate() as $key => $value) {
            switch ($key) {
                case self::NAME:
                    $user->setName($value);
                    break;
                case self::LEVEL:
                    $user->setLevel($value);
                    break;
            }
        }

        return $user;
    }
}
