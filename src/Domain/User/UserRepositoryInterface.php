<?php

namespace Example\Domain\User;

use Example\Domain\Core\Id;

/**
 * Interface UserRepositoryInterface
 *
 * @package Example\Domain\User
 */
interface UserRepositoryInterface
{
    /** 
     * @param Id $id
     *
     * @return User
     */
    public function getById(Id $id);

    /**
     * Saves a user, if it is new it assigns a user id
     * 
     * @param User $user
     */
    public function save(User $user);

    /**
     * @param User $user
     */
    public function delete(User $user);

    /**
     * @param UserName $name
     *
     * @return boolean
     */
    public function nameExists(UserName $name);

    /**
     * @param User $user
     * @param User $old
     */
    public function update(User $user, User $old);
}