<?php

namespace Example\Domain\User;

use Example\Domain\Core\EntityInterface;
use Example\Domain\Core\EntityTrait;
use Example\Domain\Core\Id;
use Example\Domain\Core\ManagedEntityInterface;
use Example\Domain\Core\ValueObject\IntegerPositiveOrZero;
use Example\Domain\Core\ValueObject\TimeStamp;

/**
 * Class User
 *
 * @package Example\Domain\User
 */
class User implements EntityInterface
{
    use EntityTrait;

    /**
     * @var UserName
     */
    private $name;
    /**
     * @var UserLevel
     */
    private $level;
    /**
     * @var IntegerZeroOrPositive
     */
    private $photoCount;
    /**
     * @var IntegerZeroOrPositive
     */
    private $videoCount;

    /**
     * User constructor.
     *
     * @param Id                    $id
     * @param UserName              $name
     * @param UserLevel             $level
     * @param IntegerPositiveOrZero $photoCount
     * @param IntegerPositiveOrZero $videoCount
     * @param TimeStamp             $lastUpdate
     */
    public function __construct(
        Id $id,
        UserName $name,
        UserLevel $level,
        IntegerPositiveOrZero $photoCount,
        IntegerPositiveOrZero $videoCount,
        TimeStamp $lastUpdate
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->level = $level;
        $this->photoCount = $photoCount;
        $this->videoCount = $videoCount;
        $this->lastUpdate = $lastUpdate;
    }

    /**
     * @return UserName
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return UserLevel
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return IntegerPositiveOrZero
     */
    public function getPhotoCounter()
    {
        return $this->photoCount;
    }

    /**
     * @return IntegerPositiveOrZero
     */
    public function getVideoCounter()
    {
        return $this->videoCount;
    }

    /**
     * Updates the lat update timestamp
     */
    public function update()
    {
        $this->lastUpdate = new TimeStamp();
    }

    /**
     * @param UserName $name
     */
    public function setName(UserName $name)
    {
        $this->name = $name;
    }

    /**
     * @param UserLevel $level
     */
    public function setLevel(UserLevel $level)
    {
        $this->level = $level;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'userId'     => $this->id->getValue(),
            'name'       => (string) $this->name,
            'level'      => (string) $this->level,
            'photoCount' => $this->photoCount->getValue(),
            'videoCount' => $this->videoCount->getValue(),
            'lastUpdate' => $this->lastUpdate->getTimestamp(),
        ];
    }
}
