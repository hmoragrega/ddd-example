<?php

namespace Example\Domain\User;

use Example\Domain\Core\ValueObject\StringBetween;

class UserName extends StringBetween
{
    private static $min = 3;
    private static $max = 50;

    /**
     * UserName constructor.
     *
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name, static::$min, static::$max, 'user name');
    }
}
