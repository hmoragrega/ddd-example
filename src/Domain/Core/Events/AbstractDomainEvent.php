<?php

namespace Example\Domain\Core\Events;

/**
 * Class AbstractDomainEvent
 *
 * @package Example\Domain\Core\Events
 */
abstract class AbstractDomainEvent implements \JsonSerializable
{
    /**
     * @var \DateTimeImmutable
     */
    private $time;

    /**
     * AbstractDomainEvent constructor.
     */
    public function __construct()
    {
        $this->time = new \DateTimeImmutable();
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getTime()
    {
        return $this->time;
    }
}
