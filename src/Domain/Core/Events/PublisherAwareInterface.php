<?php

namespace Example\Domain\Core\Events;

/**
 * Interface PublisherAwareInterface
 *
 * @package Example\Domain\Core\Command
 */
interface PublisherAwareInterface
{
    /**
     * Set the publisher in the command
     * 
     * @param PublisherInterface $publisher
     */
    public function setPublisher(PublisherInterface $publisher);
}