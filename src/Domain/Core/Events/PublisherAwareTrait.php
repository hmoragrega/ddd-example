<?php

namespace Example\Domain\Core\Events;

use Example\Domain\Core\Exception\ErrorException;

/**
 * Trait PublisherAwareTrait
 *
 * @package Example\Domain\Core\Command
 */
trait PublisherAwareTrait
{
    private $publisher;

    /**
     * Set the publisher in the command
     *
     * @param PublisherInterface $publisher
     */
    public function setPublisher(PublisherInterface $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * Gets the publisher
     * 
     * @return PublisherInterface
     */
    protected function getPublisher()
    {
        if (!$this->publisher instanceof PublisherInterface) {
            throw new ErrorException("Publisher has not been set");
        }

        return $this->publisher;
    }
}
