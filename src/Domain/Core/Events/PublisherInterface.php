<?php

namespace Example\Domain\Core\Events;

interface PublisherInterface
{
    public function publish(AbstractDomainEvent $domainEvent);
}