<?php

namespace Example\Domain\Core\Events;

interface DispatcherInterface
{
    public function subscribe($eventClass, callable $handler);

    public function subscribeAll(array $eventClasses, callable $handler);
}