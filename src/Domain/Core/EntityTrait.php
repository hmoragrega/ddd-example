<?php

namespace Example\Domain\Core;

/**
 * Class EntityTrait
 *
 * @package Example\Domain\Core
 */
use Example\Domain\Core\Exception\ValidationException;
use Example\Domain\Core\ValueObject\TimeStamp;;

/**
 * Class EntityTrait
 *
 * @package Example\Domain\Core
 */
trait EntityTrait
{
    /**
     * @var Id
     */
    private $id;

    /**
     * @var TimeStamp
     */
    private $lastUpdate;

    /**
     * @return Id
     */
    public function getId() 
    {
        return $this->id;
    }

    /**
     * @param Id $id
     *
     * @return Id
     */
    public function setId(Id $id)
    {
        if (!$this->isNew()) {
            throw new ValidationException("Only new entities are allowed to change the id");
        }

        if ($id->isNull()) {
            throw new ValidationException("You cannot assign a nul ide to an entity");
        }

        return $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->getId()->isNull();
    }

    /**
     * Updates the last update timestamp
     */
    public function setLastUpdate()
    {
        $this->lastUpdate = new TimeStamp();
    }

    /**
     * @return TimeStamp
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return __CLASS__.":{$this->getId()}";
    }
}