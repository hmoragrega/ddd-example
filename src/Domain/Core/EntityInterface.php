<?php

namespace Example\Domain\Core;

/**
 * Interface EntityInterface
 *
 * @package Example\Domain\Core
 */
interface EntityInterface extends \JsonSerializable
{
    /**
     * @return Id
     */
    public function getId();

    /**
     * @param Id $id
     *
     * @return Id
     */
    public function setId(Id $id);

    /**
     * @return boolean
     */
    public function isNew();

    /**
     * Any action that needs to be made after an entity has been updated
     */
    public function getLastUpdate();

    /**
     * @return mixed
     */
    public function __toString();
}
