<?php

namespace Example\Domain\Core;

use Example\Domain\Core\Exception\ValidationException;

/**
 * Class Validator
 *
 * @package Example\Domain\Core
 */
class Validator
{
    /**
     * @param array $constraints
     *
     * @return array
     * @throw ValidationException
     */
    public function validate(array $constraints)
    {
        $results = [];
        $errors  = [];

        foreach ($constraints as $key => $constraint) {
            $results[$key] = $this->evaluate($key, $constraint, $errors);
        }

        if (count($errors) > 0) {
            throw (new ValidationException('Errors in validation'))->setErrors($errors);
        }

        return $results;
    }

    /**
     * @param          $key
     * @param callable $constraint
     * @param array    $errors
     *
     * @return mixed
     */
    private function evaluate($key, callable $constraint, array &$errors)
    {
        try {
            return call_user_func($constraint);
        } catch (\Exception $exception) {
            $errors[$key] = $exception->getMessage();
        }
    }
}