<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\InValuesTrait;

/**
 * Class StringInValues
 *
 * @package Example\Domain\Core
 */
class StringInValues extends String
{
    use InValuesTrait;

    /**
     * StringInValues constructor.
     *
     * @param        $string
     * @param array  $validValues
     * @param string $name
     */
    public function __construct($string, array $validValues, $name = 'value')
    {
        parent::__construct($string, $name);
        $this->isInValues($name, $string, $validValues);
    }
}
