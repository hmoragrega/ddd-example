<?php

namespace Example\Domain\Core\ValueObject\Traits;

trait StringTrait
{
    use ValueObjectTrait;

    /**
     * Return the length of the string in characters
     *
     * @return int
     */
    public function count()
    {
        return mb_strlen($this->getValue());
    }

    /**
     * Return the size of the string in bytes
     *
     * @return int
     */
    public function size()
    {
        return strlen($this->getValue());
    }
}