<?php

namespace Example\Domain\Core\ValueObject\Traits;

/**
 * Class ValueObjectTrait
 *
 * @package Example\Domain\Core\ValueObject
 */
trait ValueObjectTrait
{
    /**
     * @var mixed
     */
    protected $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     */
    protected function setValue($value)
    {
        return $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->value;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->value;
    }
}
