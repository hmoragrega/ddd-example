<?php

namespace Example\Domain\Core\ValueObject\Traits;

trait NumericTrait
{
    use ValueObjectTrait;

    /**
     * Increment the integer by 1
     */
    public function increment()
    {
        $this->setValue($this->value + 1);
    }

    /**
     * Decrements the integer by one
     */
    public function decrement()
    {
        $this->setValue($this->value - 1);
    }

    /**
     * Checks if the value is positive
     *
     * @return bool
     */
    public function isPositive()
    {
        return $this->value > 0;
    }

    /**
     * Checks if the value is negative
     *
     * @return bool
     */
    public function isNegative()
    {
        return $this->value < 0;
    }

    /**
     * Checks if the value is exactly zero
     *
     * @return bool
     */
    public function isZero()
    {
        return $this->value == 0;
    }

    /**
     * @return mixed
     */
    public function count()
    {
        return $this->value;
    }
}
