<?php

namespace Example\Domain\Core\ValueObject;

/**
 * Class TimeStamp
 *
 * @package Example\Domain\Core
 */
class TimeStamp extends IntegerPositive
{
    private $dateTime;

    /**
     * IntegerPositive constructor.
     *
     * @param int    $integer
     * @param string $name
     */
    public function __construct($integer = null, $name = 'time stamp')
    {
        if (is_null($integer)) {
            $integer = time();
        }

        parent::__construct($integer, $name);
        $this->dateTime = new \DateTimeImmutable('@'.$integer);
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getTimeStamp()
    {
        return $this->dateTime->getTimestamp();
    }
}
