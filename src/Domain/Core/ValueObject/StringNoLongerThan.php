<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsStringBetweenTrait;

/**
 * Class StringBetween
 *
 * @package Example\Domain\Core
 */
class StringNoLongerThan extends String
{
    use IsStringBetweenTrait;

    /**
     * StringBetween constructor.
     *
     * @param        $string
     * @param        $max
     * @param string $name
     */
    public function __construct($string, $max, $name = 'value')
    {
        parent::__construct($string, $name);
        $this->isStringBetweenRange($name, $string, 0, $max);
    }
}
