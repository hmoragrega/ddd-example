<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Traits\StringTrait;
use Example\Domain\Core\ValueObject\Validation\IsTypeTrait;

/**
 * Class String
 *
 * @package Example\Domain\Core
 */
class String implements ValueObjectInterface, \Countable
{
    use StringTrait, IsTypeTrait;

    /**
     * String constructor.
     *
     * @param        $string
     * @param string $name
     */
    public function __construct($string, $name = 'string')
    {
        $this->isType($name, $string, ['string']);
        $this->value = $string;
    }
}
