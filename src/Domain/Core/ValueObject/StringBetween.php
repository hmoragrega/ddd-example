<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsStringBetweenTrait;

/**
 * Class StringBetween
 *
 * @package Example\Domain\Core
 */
class StringBetween extends String
{
    use IsStringBetweenTrait;

    /**
     * StringBetween constructor.
     *
     * @param        $string
     * @param string $min
     * @param        $max
     * @param string $name
     */
    public function __construct($string, $min, $max, $name = 'string')
    {
        parent::__construct($string, $name);
        $this->isStringBetweenRange($name, $string, $min, $max);
    }
}
