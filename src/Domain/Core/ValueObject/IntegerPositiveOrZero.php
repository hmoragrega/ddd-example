<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsNumberEqualOrGreaterThanTrait;

/**
 * Class IntegerPositiveOrZero
 *
 * @package Example\Domain\Core
 */
class IntegerPositiveOrZero extends Integer
{
    use IsNumberEqualOrGreaterThanTrait;

    /**
     * IntegerPositiveOrZero constructor.
     *
     * @param int    $integer
     * @param string $name
     */
    public function __construct($integer, $name = 'value')
    {
        parent::__construct($integer, $name);
        $this->isNumberEqualOrGreaterThan($name, $integer, 0);
    }
}
