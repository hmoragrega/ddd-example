<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsNumberEqualOrSmallerThanTrait;

/**
 * Class IntegerGreaterThanZero
 *
 * @package Example\Domain\Core
 */
class IntegerNegative extends Integer
{
    use IsNumberEqualOrSmallerThanTrait;

    /**
     * IntegerGreaterThanZero constructor.
     *
     * @param int    $integer
     * @param string $name
     */
    public function __construct($integer, $name = 'integer')
    {
        parent::__construct($name, $integer);
        $this->isNumberEqualOrSmallerThan($name, $integer, -1);
    }
}
