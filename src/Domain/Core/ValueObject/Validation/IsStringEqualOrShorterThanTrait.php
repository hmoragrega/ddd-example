<?php

namespace Example\Domain\Core\ValueObject\Validation;

use Example\Domain\Core\Exception\ValidationException;

/**
 * Class IsEqualOrShorterThanTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
Trait IsStringEqualOrShorterThanTrait
{
    use ValidationTrait;

    /**
     * Validates that the number is smaller then
     *
     * @param        $value
     * @param        $max
     * @param        $name
     * @param string $message
     */
    public function isStringEqualOrShorterThan(
        $name,
        $value,
        $max,
        $message = 'The {NAME} has to be {MAX} characters maximum'
    ) {
        if (!is_int($max)) {
            throw new ValidationException("The maximum length for $name is not an integer");
        }

        if (mb_strlen($value) > $max) {
            throw new ValidationException($this->format($message, ['{NAME}', '{MAX}'], [$name, $max]));
        }
    }
}
