<?php

namespace Example\Domain\Core\ValueObject\Validation;

use Example\Domain\Core\Exception\ValidationException;
use Example\Domain\Core\ValueObject\Integer;

/**
 * Class IsNumberBetweenTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
Trait IsNumberBetweenTrait
{
    use ValidationTrait, IsNumberEqualOrGreaterThanTrait, IsNumberEqualOrSmallerThanTrait;

    /**
     * @param        $value
     * @param        $min
     * @param        $max
     * @param string $name
     * @param string $message
     */
    public function isWithinRange(
        $name,
        $value,
        $min,
        $max,
        $message = 'The {NAME} valid range is between {MIN} and {MAX}'
    ) {
        $message = $this->format($message, ['{NAME}', '{MIN}', '{MAX}'], [$name, $min, $min]);
        $this->isNumberEqualOrGreaterThan($value, $min, $name, $message);
        $this->isNumberEqualOrSmallerThan($value, $max, $name, $message);
    }
}
