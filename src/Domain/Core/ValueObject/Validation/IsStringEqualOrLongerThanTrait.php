<?php

namespace Example\Domain\Core\ValueObject\Validation;

use Example\Domain\Core\Exception\ValidationException;

/**
 * Class IsEqualOrLongerThanTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
trait IsStringEqualOrLongerThanTrait
{
    use ValidationTrait;

    /**
     * Validates that the number is smaller then
     *
     * @param        $value
     * @param        $min
     * @param        $name
     * @param string $message
     */
    public function isStringEqualOrLongerThan(
        $name,
        $value,
        $min,
        $message = 'The {NAME} has to have {MIN} characters minimum'
    ) {
        if (!is_int($min)) {
            throw new ValidationException("The minimum length for $name is not an integer");
        }

        if (mb_strlen($value) < $min) {
            throw new ValidationException($this->format($message, ['{NAME}', '{MIN}'], [$name, $min]));
        }
    }
}
