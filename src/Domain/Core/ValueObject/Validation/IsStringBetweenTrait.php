<?php

namespace Example\Domain\Core\ValueObject\Validation;

/**
 * Class IsBetweenRangeTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
Trait IsStringBetweenTrait
{
    use IsStringEqualOrLongerThanTrait, IsStringEqualOrShorterThanTrait {
        IsStringEqualOrLongerThanTrait::format insteadof IsStringEqualOrShorterThanTrait;
    }

    /**
     * Validates that the number is smaller then
     *
     * @param        $value
     * @param        $min
     * @param        $max
     * @param        $name
     * @param string $message
     */
    public function isStringBetweenRange(
        $name,
        $value,
        $min,
        $max,
        $message = 'The {NAME} has to be between {MIN} and {MAX} characters'
    ) {
        $message = $this->format($message, ['{NAME}', '{MIN}', '{MAX}'], [$name, $min, $max]);
        $this->isStringEqualOrLongerThan($name, $value, $min, $message);
        $this->isStringEqualOrShorterThan($name, $value, $max, $message);
    }
}
