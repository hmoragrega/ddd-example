<?php

namespace Example\Domain\Core\ValueObject\Validation;

use Example\Domain\Core\Exception\ValidationException;

/**
 * Class IsTypeTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
trait IsTypeTrait
{
    use ValidationTrait;

    /**
     * @param              $value
     * @param string|array $types
     * @param string       $name
     * @param string       $message
     */
    protected function isType(
        $name,
        $value,
        array $types,
        $message = 'The {NAME} type is not valid. Valid types are: {TYPES}'
    ) {
        if (!in_array(gettype($value), $types)) {
            $message = $this->format($message, ['{NAME}', '{TYPES}'], [$name, implode(', ', $types)]);
            throw new ValidationException($message);
        }
    }
}
