<?php

namespace Example\Domain\Core\ValueObject\Validation;

use Example\Domain\Core\Exception\ValidationException;
use Example\Domain\Core\ValueObject\Integer;

/**
 * Class IsEqualOrGreaterThanTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
trait IsNumberEqualOrGreaterThanTrait
{
    use ValidationTrait;

    /**
     * Validates that the number is equal or greater than a minimum
     *
     * @param string $name
     * @param        $value
     * @param        $min
     * @param string $message
     */
    public function isNumberEqualOrGreaterThan(
        $name,
        $value, 
        $min,
        $message = 'The {NAME} has to be {MIN} or greater'
    ) {
        if (!is_int($min)) {
            throw new ValidationException("The minimum value for $name is not an integer");
        }

        if ($value < $min) {
            throw new ValidationException($this->format($message, ['{NAME}', '{MIN}'], [$name, $min]));
        }
    }
}
