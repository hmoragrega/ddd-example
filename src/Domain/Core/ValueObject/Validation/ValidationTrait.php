<?php

namespace Example\Domain\Core\ValueObject\Validation;

/**
 * Class ValidationTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
trait ValidationTrait
{
    /**
     * @param       $message
     * @param array $search
     * @param array $replace
     *
     * @return mixed
     */
    protected function format($message, array $search, array $replace)
    {
        return str_replace($search, $replace, $message);
    }
}
