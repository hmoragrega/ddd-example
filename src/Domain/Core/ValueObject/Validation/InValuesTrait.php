<?php

namespace Example\Domain\Core\ValueObject\Validation;

use Example\Domain\Core\Exception\ValidationException;

/**
 * Class InValuesTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
trait InValuesTrait
{
    use ValidationTrait;

    /**
     * @param string $name
     * @param        $value
     * @param array  $validValues
     * @param string $message
     */
    public function isInValues(
        $name,
        $value,
        array $validValues,
        $message = 'The {NAME} is not valid. Valid ones are: {VALUES}'
    ) {
        if (!in_array($value, $validValues)) {
            throw new ValidationException(
                $this->format($message, ['{NAME}', '{VALUES}'], [$name, implode(', ', $validValues)])
            );
        }
    }
}
