<?php

namespace Example\Domain\Core\ValueObject\Validation;

use Example\Domain\Core\Exception\ValidationException;
use Example\Domain\Core\ValueObject\Integer;

/**
 * Class IsEqualOrSmallerThanTrait
 *
 * @package Example\Domain\Core\ValueObject\Validation
 */
trait IsNumberEqualOrSmallerThanTrait
{
    use ValidationTrait;

    public function isNumberEqualOrSmallerThan(
        $name,
        $value,
        $max,
        $message = 'The {NAME} has to be {MAX} or smaller'
    ) {
        if (!is_int($max)) {
            throw new ValidationException("The maximum value for $name is not an integer");
        }

        if ($value > $max) {
            throw new ValidationException($this->format($message, ['{NAME}', '{MAX}'], [$name, $max]));
        }
    }
}
