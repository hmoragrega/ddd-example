<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Traits\NumericTrait;
use Example\Domain\Core\ValueObject\Validation\IsOfTypeTrait;
use Example\Domain\Core\ValueObject\Validation\IsTypeTrait;

/**
 * Class Integer
 *
 * @package Example\Domain\Core
 */
class Float implements ValueObjectInterface, \Countable 
{
    use NumericTrait, IsTypeTrait;

    /**
     * Integer constructor.
     *
     * @param int    $integer
     * @param string $name
     */
    public function __construct($integer, $name = 'value')
    {
        $this->isType($name, $integer, ['double']);
        $this->value = $integer;
    }
}
