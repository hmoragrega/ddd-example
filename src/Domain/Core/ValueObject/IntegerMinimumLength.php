<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsStringEqualOrLongerThanTrait;

/**
 * Class IntegerMinimumLength
 *
 * @package Example\Domain\Core
 */
class IntegerMinimumLength extends String
{
    use IsStringEqualOrLongerThanTrait;

    /**
     * IntegerMinimumLength constructor.
     *
     * @param int    $string
     * @param string $min
     * @param string $name
     */
    public function __construct($string, $min, $name = 'integer')
    {
        parent::__construct($name, $string);
        $this->isStringEqualOrLongerThan($name, $string, $min);
    }
}
