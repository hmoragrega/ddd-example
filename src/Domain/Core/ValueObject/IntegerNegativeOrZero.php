<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsNumberEqualOrSmallerThanTrait;

/**
 * Class IntegerNegativeOrZero
 *
 * @package Example\Domain\Core
 */
class IntegerNegativeOrZero extends Integer
{
    use IsNumberEqualOrSmallerThanTrait;

    /**
     * IntegerNegativeOrZero constructor.
     *
     * @param int    $integer
     * @param string $name
     */
    public function __construct($integer, $name = 'value')
    {
        parent::__construct($name, $integer);
        $this->isNumberEqualOrSmallerThan($integer, 0);
    }
}
