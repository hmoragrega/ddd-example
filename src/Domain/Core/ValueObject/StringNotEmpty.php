<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsStringEqualOrLongerThanTrait;

/**
 * Class String
 *
 * @package Example\Domain\Core
 */
class StringNotEmpty extends String
{
    use IsStringEqualOrLongerThanTrait;

    /**
     * String constructor.
     *
     * @param        $string
     * @param string $name
     */
    public function __construct($string, $name = 'value')
    {
        parent::__construct($string, $name);
        $this->isStringEqualOrLongerThan($name, $string, 1);
    }
}
