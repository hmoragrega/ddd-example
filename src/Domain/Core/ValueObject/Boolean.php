<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsTypeTrait;

/**
 * Class Boolean
 *
 * @package Example\Domain\Core\ValueObject
 */
class Boolean implements ValueObjectInterface
{
    use IsTypeTrait;

    /**
     * Boolean constructor.
     *
     * @param        $boolean
     * @param string $name
     */
    public function __construct($boolean, $name = 'value')
    {
        $this->isType($name, $boolean, ['boolean']);
        $this->setValue($boolean);
        $this->flush();
    }

    /**
     * @return bool
     */
    public function isTrue()
    {
        return $this->getValue() == true;
    }

    /**
     * @return bool
     */
    public function isFalse()
    {
        return $this->getValue() == false;
    }
}
