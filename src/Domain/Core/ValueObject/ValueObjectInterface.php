<?php

namespace Example\Domain\Core\ValueObject;

interface ValueObjectInterface extends \JsonSerializable
{
    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return mixed
     */
    public function __toString();
}
