<?php

namespace Example\Domain\Core\ValueObject;

use Example\Domain\Core\ValueObject\Validation\IsNumberEqualOrGreaterThanTrait;

/**
 * Class IntegerPositive
 *
 * @package Example\Domain\Core
 */
class IntegerPositive extends Integer
{
    use IsNumberEqualOrGreaterThanTrait;

    /**
     * IntegerPositive constructor.
     *
     * @param int    $integer
     * @param string $name
     */
    public function __construct($integer, $name = 'integer')
    {
        parent::__construct($integer, $name);
        $this->isNumberEqualOrGreaterThan($name, $integer, 1);
    }
}
