<?php

namespace Example\Domain\Core;

use Example\Domain\Core\ValueObject\Traits\ValueObjectTrait;
use Example\Domain\Core\ValueObject\Validation\IsNumberEqualOrGreaterThanTrait;
use Example\Domain\Core\ValueObject\Validation\IsTypeTrait;
use Example\Domain\Core\ValueObject\ValueObjectInterface;

/**
 * Class Id
 *
 * @package Example\Domain\Core\ValueObject
 */
class Id implements ValueObjectInterface
{
    use ValueObjectTrait, IsTypeTrait, IsNumberEqualOrGreaterThanTrait {
        IsTypeTrait::format insteadof IsNumberEqualOrGreaterThanTrait;
    }

    public function __construct($integer, $name = 'id')
    {
        if (!is_null($integer)) {
            $this->isType($name, $integer, ['null', 'integer']);
            $this->isNumberEqualOrGreaterThan($name, $integer, 1);
        }

        $this->value = $integer;
    }

    /**
     * @return bool
     */
    public function isNull()
    {
        return $this->value === null;
    }
}
