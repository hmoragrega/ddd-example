<?php

namespace Example\Domain\Core\Transactional;

/**
 * Interface TransactionalObservableSystemInterface
 *
 * @package Example\Domain\Core\Transactional
 */
interface TransactionalObservableSystemInterface extends TransactionalSystemInterface
{
    /**
     * @param TransactionObserverInterface $observer
     */
    public function attachObserver(TransactionObserverInterface $observer);
}