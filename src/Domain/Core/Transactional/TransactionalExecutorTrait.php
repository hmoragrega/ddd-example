<?php

namespace Example\Domain\Core\Transactional;

/**
 * Trait AbstractTransactionalExecutor
 *
 * @package Example\Domain\Core\Command
 */
trait TransactionalExecutorTrait
{
    /**
     * @param TransactionalSystemInterface $transactionalSystem
     * @param callable                     $transaction
     *
     * @return mixed
     * @throws \Exception
     */
    protected function executeTransactional(TransactionalSystemInterface $transactionalSystem, callable $transaction)
    {
        $transactionalSystem->startTransaction();
        try {
            $result = call_user_func($transaction);
            $transactionalSystem->commit();

        } catch (\Exception $exception) {
            $transactionalSystem->rollback();
            throw $exception;
        }

        return $result;
    }
}
