<?php

namespace Example\Domain\Core\Transactional;

/**
 * Interface TransactionalAwareInterface
 *
 * @package Example\Domain\Core\Transactional
 */
interface TransactionalAwareInterface
{
    /**
     * @param TransactionalSystemInterface $transactional
     */
    public function setTransactionalSystem(TransactionalSystemInterface $transactional);
}