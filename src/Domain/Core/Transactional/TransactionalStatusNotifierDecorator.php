<?php

namespace Example\Domain\Core\Transactional;

/**
 * Class TransactionalStatusNotifierDecorator
 *
 * @package Example\Domain\Core\Transactional
 */
class TransactionalStatusNotifierDecorator implements TransactionalSystemInterface
{
    /**
     * @var TransactionalSystemInterface
     */
    private $transactionalSystem;

    /**
     * @var TransactionObserverInterface[]
     */
    private $observers;

    /**
     * TransactionalStatusNotifierDecorator constructor.
     *
     * @param TransactionalSystemInterface $transactionalSystem
     */
    public function __construct(TransactionalSystemInterface $transactionalSystem)
    {
        $this->transactionalSystem = $transactionalSystem;
    }

    /**
     * Notifies all the observers and starts the transaction
     */
    function startTransaction()
    {
        foreach ($this->observers as $observer) {
            $observer->transactionHasStarted();
        }

        $this->transactionalSystem->startTransaction();
    }

    /**
     * Commits the transaction and notifies the observers
     */
    public function commit()
    {
        $this->transactionalSystem->commit();

        foreach ($this->observers as $observer) {
            $observer->transactionHasBeenCommitted();
        }
    }

    /**
     * Rollbacks the transaction and notifies the observers
     */
    public function rollback()
    {
        $this->transactionalSystem->rollback();

        foreach ($this->observers as $observer) {
            $observer->transactionHasBeenRolledBack();
        }
    }

    /**
     * Attach an observer to the transactional status notifier
     * 
     * @param TransactionObserverInterface $observer
     */
    public function attachObserver(TransactionObserverInterface $observer)
    {
        $this->observers[] = $observer;
    }
}
