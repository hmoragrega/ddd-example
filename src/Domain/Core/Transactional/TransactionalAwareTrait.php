<?php

namespace Example\Domain\Core\Transactional;

use Example\Domain\Core\Exception\ErrorException;

/**
 * Class TransactionalAwareTrait
 *
 * @package Example\Domain\Core\Transactional
 */
trait TransactionalAwareTrait
{
    /**
     * @var TransactionalSystemInterface
     */
    private $transactionalSystem;

    /**
     * Set the transactional system in the command
     *
     * @param TransactionalSystemInterface $transactionalSystem
     */
    public function setTransactionalSystem(TransactionalSystemInterface $transactionalSystem)
    {
        $this->transactionalSystem = $transactionalSystem;
    }

    /**
     * @return TransactionalSystemInterface
     */
    protected function getTransactionalSystem()
    {
        if (!$this->transactionalSystem instanceof TransactionalSystemInterface) {
            throw new ErrorException("Transactional system has been not set");
        }

        return $this->transactionalSystem;
    }
}
