<?php

namespace Example\Domain\Core\Transactional;

/**
 * Interface TransactionObserverInterface
 *
 * @package Example\Domain\Core\Transactional
 */
interface TransactionObserverInterface
{
    /**
     * It gets notified that a transaction has started
     */
    public function transactionHasStarted();

    /**
     * Callback that gets executed when a transaction has been committed
     */
    public function transactionHasBeenCommitted();

    /**
     * Callback tht gets executed when a transaction has been rolled back
     */
    public function transactionHasBeenRolledBack();
}