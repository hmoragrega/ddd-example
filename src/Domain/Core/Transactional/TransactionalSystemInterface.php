<?php

namespace Example\Domain\Core\Transactional;

/**
 * Interface TransactionalSystemInterface
 *
 * @package Example\Domain\Core\Transactional
 */
interface TransactionalSystemInterface
{
    /**
     * Starts a transaction
     */
    public function startTransaction();

    /**
     * Commits a transaction
     */
    public function commit();

    /**
     * Rollbacks a transaction
     */
    public function rollback();
}
