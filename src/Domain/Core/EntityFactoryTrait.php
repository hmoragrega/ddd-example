<?php

namespace Example\Domain\Core;

/**
 * Class EntityFactoryTrait
 *
 * @package Example\Domain\Core
 */
trait EntityFactoryTrait
{
    /**
     * Validator for validate the entities
     *
     * @var Validator
     */
    private $validator;

    /**
     * Constraint to apply for the next build entity
     *
     * @var array
     */
    protected $constraints = [];

    /**
     * EntityFactoryTrait constructor.
     *
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    private function validate()
    {
        $constraints = $this->constraints;
        $this->reset();

        return $this->validator->validate($constraints);
    }

    /**
     * Resets the constraints to apply
     */
    public function reset()
    {
        $this->constraints = [];
    }
}
