<?php

namespace Example\Domain\Core;

interface EntityClonerInterface
{
    /**
     * Gets a deep copy of the entity
     *
     * @param EntityInterface $entity
     *
     * @return EntityInterface $entity
     */
    public function copy(EntityInterface $entity);
}
