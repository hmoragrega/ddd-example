<?php

namespace Example\Domain\Core\Cache;

/**
 * Interface CacheAwareInterface
 *
 * @package Example\Domain\Core\Cache
 */
interface CacheAwareInterface
{
    /**
     * @param CacheInterface $cache
     */
    public function setCache(CacheInterface $cache);
}
