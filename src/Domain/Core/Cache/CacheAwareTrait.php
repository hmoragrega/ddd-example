<?php

namespace Example\Domain\Core\Cache;

use Example\Domain\Core\Exception\ErrorException;

/**
 * Class CacheAwareTrait
 *
 * @package Example\Domain\Core\Cache
 */
trait CacheAwareTrait
{
    private $cache;

    /**
     * @param CacheInterface $cache
     */
    public function setCache(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Returns the cache
     * 
     * @return mixed
     */
    protected function getCache()
    {
        if (!$this->cache instanceof CacheInterface) {
            throw new ErrorException('Cache has not been set');
        }

        return $this->cache;
    }
}
