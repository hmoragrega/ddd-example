<?php

namespace Example\Domain\Core\Cache;

/**
 * Interface CacheInterface
 *
 * @package Example\Domain\Core\Cache
 */
interface CacheInterface
{
    /**
     * @param      $key
     * @param      $value
     * @param null $timeToLive
     *
     * @return mixed
     */
    public function store($key, $value, $timeToLive = null);

    /**
     * @param $key
     *
     * @return mixed|null Null if not present or expired
     */
    public function retrieve($key);

    /**
     * @param $key
     *
     * @return mixed
     */
    public function remove($key);
}