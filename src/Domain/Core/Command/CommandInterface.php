<?php

namespace Example\Domain\Core\Command;

use Example\Domain\Core\ParametersInterface;

/**
 * Interface CommandInterface
 *
 * @package Example\Domain\Core\Command
 */
interface CommandInterface
{
    /**
     * @param ParametersInterface $parameters
     *
     * @return mixed
     */
    public function execute(ParametersInterface $parameters);
}