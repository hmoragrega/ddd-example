<?php

namespace Example\Domain\Core\Command;

interface ObservableTransactionalSystemInterface extends TransactionalSystemInterface
{
    public function attachObserver(TransactionObserverInterface $observer);
}