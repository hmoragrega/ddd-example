<?php

namespace Example\Domain\Core\Command;

use Example\Domain\Core\Transactional\TransactionalAwareInterface;
use Example\Domain\Core\Transactional\TransactionalAwareTrait;
use Example\Domain\Core\Transactional\TransactionalExecutorTrait;

/**
 * Class AbstractTransactionalCommand
 *
 * @package Example\Domain\Core\Command
 */
abstract class AbstractTransactionalCommand implements CommandInterface, TransactionalAwareInterface
{
    use TransactionalAwareTrait, TransactionalExecutorTrait;

    /**
     * @param callable $transaction
     *
     * @return mixed
     * @throws \Exception
     */
    protected function executeTransaction(callable $transaction)
    {
        return $this->executeTransactional($this->getTransactionalSystem(), $transaction);
    }
}