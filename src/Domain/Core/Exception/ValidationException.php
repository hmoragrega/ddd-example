<?php

namespace Example\Domain\Core\Exception;

/**
 * Class ValidationException
 *
 * @package Example\Domain\Core\Exception
 */
class ValidationException extends ErrorException
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * ValidationException constructor.
     *
     * @param string    $message
     * @param int       $code
     * @param Exception $previous
     * @param array     $errors
     */
    public function __construct($message = "", $code = 0, Exception $previous = null, array $errors = [])
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
    }

    /**
     * @param array $errors
     * 
     * @return $this
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
        
        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $key
     *
     * @return array
     */
    public function getErrorsFor($key)
    {
        return isset($this->errors[$key]) ? $this->errors[$key] : [];
    }
}
