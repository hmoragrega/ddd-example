<?php

namespace Example\Domain\Core\Exception;

/**
 * Class ErrorException
 *
 * @package Example\Domain\Core\Exception
 */
class ErrorException extends \RuntimeException 
{

}