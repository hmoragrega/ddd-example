<?php

namespace Example\Domain\Core\Exception;

/**
 * Class ConnectionFailureException
 *
 * @package Example\Domain\Core\Exception
 */
class ConnectionFailureException extends CriticalException
{

}