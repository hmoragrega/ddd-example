<?php

namespace Example\Domain\Core\Exception;

/**
 * Class CriticalException
 *
 * @package Example\Domain\Core\Exception
 */
class CriticalException extends \RuntimeException
{

}