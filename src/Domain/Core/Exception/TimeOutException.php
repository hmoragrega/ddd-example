<?php

namespace Example\Domain\Core\Exception;

/**
 * Class TimeOutException
 *
 * @package Example\Domain\Core\Exception
 */
class TimeOutException extends CriticalException
{

}