<?php

namespace Example\Domain\Core\Exception;

/**
 * Class EntityNotFoundException
 *
 * @package Example\Domain\Core\Exception
 */
class EntityNotFoundException extends ErrorException
{

}