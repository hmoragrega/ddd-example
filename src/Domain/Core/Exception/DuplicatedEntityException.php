<?php

namespace Example\Domain\Core\Exception;

/**
 * Class DuplicatedEntityException
 *
 * @package Example\Domain\Core\Exception
 */
class DuplicatedEntityException extends ValidationException
{

}