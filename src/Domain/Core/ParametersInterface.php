<?php

namespace Example\Domain\Core;

/**
 * Interface ParametersInterface
 *
 * @package Example\Domain\Core
 */
interface ParametersInterface
{
    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * @param      $key
     *
     * @return mixed
     */
    public function demand($key);

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value);

    /**
     * @param $key
     *
     * @return bool
     */
    public function has($key);
}