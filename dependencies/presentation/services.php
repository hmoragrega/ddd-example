<?php

$app['presentation.template_engine'] = $app->share(function() use($app) {
    return new \Example\Infrastructure\Presentation\Web\TwigEngine($app['twig']);
});