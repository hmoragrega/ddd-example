<?php

$app['api.user.controller'] = $app->share(function() use ($app) {
    return new \Example\Presentation\Api\User\UserController($app['command.locator']);
});
$app['api.photo.controller'] = $app->share(function() use ($app) {
    return new \Example\Presentation\Api\Media\PhotoController($app['command.locator']);
});

$app['web.user.controller'] = $app->share(function() use ($app) {
    return new \Example\Presentation\Web\User\ProfileController($app['presentation.template_engine'], $app['use.case.locator']);
});