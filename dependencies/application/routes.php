<?php

$app->post('api/users', 'api.user.controller:create');
$app->delete('api/users/{userId}', 'api.user.controller:delete')->assert('userId', '\d+');
$app->patch('api/users/{userId}', 'api.user.controller:update')->assert('userId', '\d+');
$app->get('api/users/{userId}/profile', 'api.user.controller:getUserProfile')->assert('userId', '\d+');
$app->get('api/users/{userId}/photos', 'api.photo.controller:getAllByUser')->assert('userId', '\d+');
$app->post('api/users/{userId}/photos', 'api.photo.controller:create')->assert('userId', '\d+');;