<?php

$app['application.logger'] = $app->share(function() use($app) {
    $logger = new \Monolog\Logger('default');
    $logger->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__.'/../../log/default.log'));
    $logger->debug("\n * * * * New Request * * * *");
    return $logger;
});

$app['application.cache'] = $app->share(function() use($app) {
    return new \Example\Infrastructure\Application\Cache\RedisCache();
});

$app['application.monitor'] = $app->share(function() use($app) {
    return new \Example\Infrastructure\Application\Monitoring\DummyMonitor();
});
