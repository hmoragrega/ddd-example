<?php

$app['validator.factory'] = $app->protect(function() {
    return new \Example\Domain\Core\Validator();
});

$app['user.factory'] = $app->share(function() use($app) {
    return new \Example\Infrastructure\Domain\User\FileUserFactory($app['validator.factory']());
});

$app['photo.factory'] = $app->share(function() use($app) {
    return new \Example\Domain\Media\PhotoFactory($app['validator.factory']());
});

$app['video.factory'] = $app->share(function() use($app) {
    return new \Example\Domain\Media\VideoFactory($app['validator.factory']());
});