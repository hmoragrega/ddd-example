<?php

$app['entity.cloner'] = $app->share(function() use($app) {
    return new \Example\Infrastructure\Domain\Core\DeepEntityCloner(new \DeepCopy\DeepCopy());
});

$app['user.repository'] = $app->share(function() use ($app) {
    return $app['user.file.repository'];
});

$app['user.file.repository'] = $app->share(function() use ($app) {
    return new \Example\Infrastructure\Domain\User\FileUserRepository(
        new SplFileObject(realpath(__DIR__.'/../../fixtures/users.json'), 'r+'),
        $app['user.factory'],
        $app['application.logger']
    );
});

$app['photo.repository'] = $app->share(function() use ($app) {
    return new \Example\Infrastructure\Domain\Media\FilePhotoRepository(
        new SplFileObject(realpath(__DIR__.'/../../fixtures/photos.json'), 'r+'),
        $app['photo.factory']
    );
});

$app['video.repository'] = $app->share(function() use ($app) {
    return new \Example\Infrastructure\Domain\Media\FileVideoRepository(
        new SplFileObject(realpath(__DIR__.'/../../fixtures/videos.json'), 'r+'),
        $app['video.factory']
    );
});