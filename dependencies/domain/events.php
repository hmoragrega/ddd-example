<?php

$eventService = $app->share(function() use($app) {
    $events = new \Example\Infrastructure\Domain\Core\Events\SynchronousEventService($app['application.logger']);
    $app['domain.transactional']->attachObserver($events);
    return $events;
});

$app['event.publisher']  = $eventService;
$app['event.dispatcher'] = $eventService;

/** @var \Example\Domain\Core\Events\DispatcherInterface $eventDispatcher */
$eventDispatcher = $app['event.dispatcher'];

