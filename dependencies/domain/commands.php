<?php

$commands = new Pimple();

// Builds a command injecting the required common dependencies
$app['command.factory'] = $app->share(function() use ($app) {
    return function(\Example\Domain\Core\Command\CommandInterface $command) use($app) {
        if ($command instanceof \Example\Domain\Core\Events\PublisherAwareInterface) {
            $command->setPublisher($app['event.publisher']);
        }
        if ($command instanceof \Example\Domain\Core\Transactional\TransactionalAwareInterface) {
            $command->setTransactionalSystem($app['domain.transactional']);
        }
        $command = new \Example\Application\MonitoringCommandDecorator(
            'ddd.command', 
            $command, 
            $app['application.logger'], 
            $app['application.monitor']
        );

        return $command;
    };
});

// Command locator for controllers
$app['command.locator'] = $app->share(function() use($commands) {
    return new \Example\Infrastructure\Application\DependencyInjection\PimpleServiceLocator($commands);
});

/********* USER COMMANDS ********/

// Create user
$commands['command.user.create'] = $commands->share(function() use ($app) {
    return $app['command.factory'](new \Example\Domain\User\Command\UserCreateCommand(
        $app['user.factory'],
        $app['user.repository']
    ));
});

// Delete user
$commands['command.user.delete'] = $commands->share(function() use ($app) {
    return $app['command.factory'](new \Example\Domain\User\Command\UserDeleteCommand(
        $app['user.repository'],
        $app['photo.repository'],
        $app['video.repository']
    ));
});

// Update user
$commands['command.user.update'] = $commands->share(function() use ($app) {
    return $app['command.factory'](new \Example\Domain\User\Command\UserUpdateCommand(
        $app['entity.cloner'],
        $app['user.repository'],
        $app['user.factory']
    ));
});

// Get a user profile
$commands['command.user.getUserProfile'] = $commands->share(function() use ($app) {
    return $app['command.factory'](new \Example\Domain\User\Command\GetUserProfileCommand(
        $app['user.repository'],
        $app['photo.repository'],
        $app['video.repository']
    ));
});

/********* PHOTO COMMANDS ********/

// Create photo
$commands['command.photo.create'] = $commands->share(function() use ($app) {
    return $app['command.factory'](new \Example\Domain\Media\Command\CreatePhotoCommand(
        $app['user.repository'],
        $app['photo.repository'],
        $app['photo.factory']
    ));
});

// Get all photos by user
$commands['command.photo.getAllByUser'] = $commands->share(function() use ($app) {
    return $app['command.factory'](new \Example\Domain\Media\Command\GetPhotosByUserCommand(
        $app['user.repository'],
        $app['photo.repository']
    ));
});
