<?php

$app['domain.transactional'] = $app->share(function() use($app) {
    $system = new \Example\Infrastructure\Domain\Core\Transactional\FileTransactionalSystem($app['application.logger']);
    return new \Example\Domain\Core\Transactional\TransactionalStatusNotifierDecorator($system);
});