<?php

require_once 'vendor/autoload.php';

$filename = __DIR__.preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
if (php_sapi_name() === 'cli-server' && is_file($filename)) {
    return false;
}

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), ['twig.path' => realpath(__DIR__.'/../views')]);

$app->before(function (\Symfony\Component\HttpFoundation\Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

require 'dependencies/application/routes.php';
require 'dependencies/application/services.php';
require 'dependencies/domain/commands.php';
require 'dependencies/domain/factories.php';
require 'dependencies/domain/repositories.php';
require 'dependencies/domain/transactional.php';
require 'dependencies/presentation/controllers.php';
require 'dependencies/presentation/services.php';

// Subscribe to events
require 'dependencies/domain/events.php';

$app->run();